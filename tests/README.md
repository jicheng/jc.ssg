## Unit Test

Enter this project directory and perform the unit tests through:

    python -m unittest -v

Fore more details check the documentation through

    python -m unittest -h
