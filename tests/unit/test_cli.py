import io
import logging
import unittest
from argparse import Namespace
from unittest import mock

from jicheng import cli

from .. import parametrize


class TestCli(unittest.TestCase):
    @parametrize(
        'argv, expected',
        (
            # main options
            (
                ['-r', 'myroot', 'generate'],
                Namespace(config=None, force=False, func=cli.generate, root='myroot', verbosity=logging.INFO),
            ),
            (
                ['--root', 'myroot2', 'generate'],
                Namespace(config=None, force=False, func=cli.generate, root='myroot2', verbosity=logging.INFO),
            ),
            (
                ['-c', 'my.file', 'generate'],
                Namespace(config='my.file', force=False, func=cli.generate, root='', verbosity=logging.INFO),
            ),
            (
                ['--config', 'my.file2', 'generate'],
                Namespace(config='my.file2', force=False, func=cli.generate, root='', verbosity=logging.INFO),
            ),
            (
                ['-q', 'generate'],
                Namespace(config=None, force=False, func=cli.generate, root='', verbosity=logging.WARNING),
            ),
            (
                ['--quiet', 'generate'],
                Namespace(config=None, force=False, func=cli.generate, root='', verbosity=logging.WARNING),
            ),
            (
                ['-v', 'generate'],
                Namespace(config=None, force=False, func=cli.generate, root='', verbosity=logging.DEBUG),
            ),
            (
                ['--verbose', 'generate'],
                Namespace(config=None, force=False, func=cli.generate, root='', verbosity=logging.DEBUG),
            ),

            # require command
            (
                [],
                SystemExit,
            ),

            # generate
            (
                ['g'],
                Namespace(config=None, force=False, func=cli.generate, root='', verbosity=logging.INFO),
            ),
            (
                ['generate'],
                Namespace(config=None, force=False, func=cli.generate, root='', verbosity=logging.INFO),
            ),
            (
                ['generate', '--force'],
                Namespace(config=None, force=True, func=cli.generate, root='', verbosity=logging.INFO),
            ),

            # clean
            (
                ['clean'],
                Namespace(config=None, func=cli.clean, root='', verbosity=logging.INFO),
            ),

            # parse
            (
                ['p'],
                Namespace(config=None, ifile=None, imode='html', ofile=None, omode='html', func=cli.parse, root='', verbosity=logging.INFO),
            ),
            (
                ['parse'],
                Namespace(config=None, ifile=None, imode='html', ofile=None, omode='html', func=cli.parse, root='', verbosity=logging.INFO),
            ),
            (
                ['parse', '-i', 'my.file'],
                Namespace(config=None, ifile='my.file', imode='html', ofile=None, omode='html', func=cli.parse, root='', verbosity=logging.INFO),
            ),
            (
                ['parse', '-l', 'mymode'],
                Namespace(config=None, ifile=None, imode='mymode', ofile=None, omode='html', func=cli.parse, root='', verbosity=logging.INFO),
            ),
            (
                ['parse', '-o', 'my.file'],
                Namespace(config=None, ifile=None, imode='html', ofile='my.file', omode='html', func=cli.parse, root='', verbosity=logging.INFO),
            ),
            (
                ['parse', '-r', 'mymode'],
                Namespace(config=None, ifile=None, imode='html', ofile=None, omode='mymode', func=cli.parse, root='', verbosity=logging.INFO),
            ),
        ),
    )
    @mock.patch('jicheng.cli.sys.stderr')
    def test_parse_args(self, argv, expected, _):
        """Test whether input argv gets expected result.

        Args:
            argv (list): The input CLI arguments.
            expected: The parsed CLI arguments as argparse.Namespace or
                a subclass of Exception that is expected to be raised.
        """
        try:
            expect_exception = issubclass(expected, BaseException)
        except TypeError:
            expect_exception = False

        if expect_exception:
            with self.assertRaises(expected):
                cli.parse_args(argv)
        else:
            self.assertEqual(expected, cli.parse_args(argv))

    @parametrize(
        'argv, expected',
        (
            (
                Namespace(config=None, force=False, root='', verbosity=logging.INFO),
                (
                    mock.call.log.setLevel(logging.INFO),
                    mock.call.StaticSiteGenerator(root='', conf={'my': 'conf'}),
                    mock.call.StaticSiteGenerator().run(force=False),
                ),
            ),
            (
                Namespace(config='myconffile', force=True, root='mydir', verbosity=logging.DEBUG),
                (
                    mock.call.log.setLevel(logging.DEBUG),
                    mock.call.StaticSiteGenerator(root='mydir', conf={'my': 'conf'}),
                    mock.call.StaticSiteGenerator().run(force=True),
                ),
            ),
        ),
    )
    @mock.patch('jicheng.cli._load_config_from_args', return_value={'my': 'conf'})
    @mock.patch('jicheng.cli._generate')
    def test_generate(self, argv, expected, m_gen, m_conf):
        cli.generate(argv)
        m_conf.assert_called_once_with(argv)
        m_gen.assert_has_calls(expected)

    @parametrize(
        'argv, expected',
        (
            (
                Namespace(config=None, root='', verbosity=logging.INFO),
                (
                    mock.call.log.setLevel(logging.INFO),
                    mock.call.StaticSiteGenerator(root='', conf={'my': 'conf'}),
                    mock.call.StaticSiteGenerator().clean(),
                ),
            ),
        ),
    )
    @mock.patch('jicheng.cli._load_config_from_args', return_value={'my': 'conf'})
    @mock.patch('jicheng.cli._generate')
    def test_clean(self, argv, expected, m_gen, m_conf):
        cli.clean(argv)
        m_conf.assert_called_once_with(argv)
        m_gen.assert_has_calls(expected)

    @mock.patch('jicheng.cli._syntaxparser')
    @mock.patch('jicheng.cli._load_config_from_args', return_value={'my': 'conf'})
    @mock.patch('jicheng.cli.sys.stderr')
    def test_parse(self, _, m_conf, m_parser):
        argv = Namespace(config=None, ifile='myroot/input.file', imode='html', ofile='myroot/output.file', omode='html')

        m_parser.Parser.return_value.output = '<p>output</p>'

        _m_open_fh = io.BytesIO(b'<p>input</p>')

        def _m_open(_, mode, *args, **kwargs):
            return _m_open_fh if mode == 'rb' else mock.DEFAULT

        with mock.patch('jicheng.cli.open', side_effect=_m_open) as m_open:
            cli.parse(argv)

        m_conf.assert_called_once_with(argv)
        m_parser.assert_has_calls([
            mock.call.Parser(char_subst_table=None, renderer_filter=None),
            mock.call.Parser().run(_m_open_fh, imode='html', omode='html'),
        ])
        m_open.assert_has_calls([
            mock.call('myroot/input.file', 'rb'),
            mock.call('myroot/output.file', 'w', encoding='UTF-8', newline=''),
            mock.call().__enter__(),
            mock.call().__enter__().write('<p>output</p>'),
        ])

    @mock.patch('jicheng.cli._syntaxparser')
    @mock.patch('jicheng.cli._load_config_from_args', return_value={'my': 'conf'})
    @mock.patch('jicheng.cli.sys.stderr')
    def test_parse_stdin(self, _, m_conf, m_parser):
        argv = Namespace(config=None, ifile=None, imode='html', ofile='myroot/output.file', omode='html')

        m_parser.Parser.return_value.output = '<p>output</p>'

        _m_bytesio = io.BytesIO(b'<p>input</p>')
        with mock.patch('io.BytesIO', return_value=_m_bytesio) as m_bytesio, \
             mock.patch('jicheng.cli.sys.stdin') as m_stdin, \
             mock.patch('jicheng.cli.open') as m_open:
            m_stdin.buffer.read.return_value = b'<p>stdin</p>'
            cli.parse(argv)

        m_conf.assert_called_once_with(argv)
        m_parser.assert_has_calls([
            mock.call.Parser(char_subst_table=None, renderer_filter=None),
            mock.call.Parser().run(_m_bytesio, imode='html', omode='html'),
        ])
        m_open.assert_has_calls([
            mock.call('myroot/output.file', 'w', encoding='UTF-8', newline=''),
            mock.call().__enter__(),
            mock.call().__enter__().write('<p>output</p>'),
        ])
        m_stdin.assert_has_calls([
            mock.call.buffer.read(),
        ])
        m_bytesio.assert_called_once_with(b'<p>stdin</p>')

    @mock.patch('jicheng.cli._syntaxparser')
    @mock.patch('jicheng.cli._load_config_from_args', return_value={'my': 'conf'})
    @mock.patch('jicheng.cli.sys.stderr')
    def test_parse_stdout(self, _, m_conf, m_parser):
        argv = Namespace(config=None, ifile='myroot/input.file', imode='html', ofile=None, omode='html')

        m_parser.Parser.return_value.output = '<p>output</p>'

        _m_open_fh = io.BytesIO(b'<p>input</p>')

        def _m_open(_, mode, *args, **kwargs):
            return _m_open_fh if mode == 'rb' else mock.DEFAULT

        with mock.patch('jicheng.cli.sys.stdout') as m_stdout, \
             mock.patch('jicheng.cli.open', side_effect=_m_open) as m_open:
            cli.parse(argv)

        m_conf.assert_called_once_with(argv)
        m_parser.assert_has_calls([
            mock.call.Parser(char_subst_table=None, renderer_filter=None),
            mock.call.Parser().run(_m_open_fh, imode='html', omode='html'),
        ])
        m_open.assert_called_once_with('myroot/input.file', 'rb')
        m_stdout.assert_has_calls([
            mock.call.write('<p>output</p>'),
        ])

    @mock.patch('jicheng.cli.open')
    @mock.patch('jicheng.cli._syntaxparser')
    @mock.patch('jicheng.cli._load_config_from_args', return_value={
        'theme': 'mytheme',
        'assets': {
            'char_subst_table': {
                '内': {'古': '内', '今': '內'},
            },
            'renderer_filter': None,
        },
        'theme_assets': {
            'mytheme': {
                'renderer_filter': [
                    {
                        'include': [{'attrs': {'data-rev': '古版'}}],
                        'action': 'hide',
                    },
                ],
            },
        },
    })
    @mock.patch('jicheng.cli.sys.stderr')
    def test_parse_assets(self, _, m_conf, m_parser, m_open):
        """Test if config assets are taken."""
        argv = Namespace(config=None, ifile='myroot/input.file', imode='html', ofile='myroot/output.file', omode='html')

        cli.parse(argv)

        m_conf.assert_called_once_with(argv)
        m_parser.assert_has_calls([
            mock.call.Parser(
                char_subst_table={'内': {'古': '内', '今': '內'}},
                renderer_filter=[{'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide'}],
            ),
            mock.call.Parser().run(mock.ANY, imode='html', omode='html'),
        ])
