import json
import os
import tempfile
import unittest
from time import sleep
from types import SimpleNamespace
from unittest import mock

from jicheng import fchecksum
from jicheng.generate import (
    CacheHandler,
    PathMap,
    PathMapEntry,
    StaticSiteGenerator,
)

from .. import TEMP_DIR, parametrize


def setUpModule():
    """Set up a temp directory for testing."""
    global _tmpdir, tmpdir
    _tmpdir = tempfile.TemporaryDirectory(prefix='gen-', dir=TEMP_DIR)
    tmpdir = _tmpdir.name


def tearDownModule():
    """Cleanup the temp directory."""
    _tmpdir.cleanup()


class TestPathMap(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Define a directory path for testing."""
        cls.root = tmpdir

    def general_test_case(self,
                          fsrc=None, fdst=None,
                          fsrc2=None, fdst2=None,
                          exists=True):
        """Provide a general test case with consistent attributes."""
        fsrc_root = os.path.join(self.root, 'source')
        fdst_root = os.path.join(self.root, 'public')

        fsrc = os.path.join(fsrc_root, 'subdir', 'page.html') if fsrc is None else fsrc
        fdst = os.path.join(fdst_root, 'page.html') if fdst is None else fdst
        fsrc_subpath = os.path.relpath(fsrc, fsrc_root)
        fdst_subpath = os.path.relpath(fdst, fdst_root)

        fsrc2 = os.path.join(fsrc_root, 'subdir', 'page2.html') if fsrc2 is None else fsrc2
        fdst2 = os.path.join(fdst_root, 'page2.html') if fdst2 is None else fdst2
        fsrc_subpath2 = os.path.relpath(fsrc2, fsrc_root)
        fdst_subpath2 = os.path.relpath(fdst2, fdst_root)

        entry = PathMapEntry(
            fsrc,
            fsrc_root,
            fsrc_subpath,
            fdst,
            fdst_root,
            fdst_subpath,
            exists,
        )
        entry2 = PathMapEntry(
            fsrc2,
            fsrc_root,
            fsrc_subpath2,
            fdst2,
            fdst_root,
            fdst_subpath2,
            exists,
        )
        entry_dummy = PathMapEntry(
            None,
            None,
            None,
            None,
            None,
            None,
            False,
        )

        return SimpleNamespace(**locals())

    @mock.patch('jicheng.generate.log.warning')
    def test_add_basic(self, mocker):
        ns = self.general_test_case()

        map_ = PathMap(ns.fsrc_root, ns.fdst_root)
        map_.add(ns.fsrc, ns.fdst)

        expected = {
            ('src', ns.fsrc): ns.entry,
            ('src_subpath', ns.fsrc_subpath): ns.entry,
            ('dst', ns.fdst): ns.entry,
            ('dst_subpath', ns.fdst_subpath): ns.entry,
        }

        self.assertEqual(expected, dict(map_))
        mocker.assert_not_called()

    @mock.patch('jicheng.generate.log.warning')
    def test_add_entry(self, mocker):
        ns = self.general_test_case()

        map_ = PathMap(ns.fsrc_root, ns.fdst_root)
        map_.add(ns.fsrc, ns.fdst)
        map_.add(ns.fsrc2, ns.fdst2)

        expected = {
            ('src', ns.fsrc): ns.entry,
            ('src_subpath', ns.fsrc_subpath): ns.entry,
            ('dst', ns.fdst): ns.entry,
            ('dst_subpath', ns.fdst_subpath): ns.entry,
            ('src', ns.fsrc2): ns.entry2,
            ('src_subpath', ns.fsrc_subpath2): ns.entry2,
            ('dst', ns.fdst2): ns.entry2,
            ('dst_subpath', ns.fdst_subpath2): ns.entry2,
        }

        self.assertEqual(expected, dict(map_))
        mocker.assert_not_called()

    @mock.patch('jicheng.generate.log.warning')
    def test_add_entry_nonexist(self, mocker):
        ns = self.general_test_case(exists=False)

        map_ = PathMap(ns.fsrc_root, ns.fdst_root)
        map_.add(ns.fsrc, ns.fdst, exists=False)
        map_.add(ns.fsrc2, ns.fdst2, exists=False)

        expected = {
            ('src', ns.fsrc): ns.entry,
            ('src_subpath', ns.fsrc_subpath): ns.entry,
            ('dst', ns.fdst): ns.entry,
            ('dst_subpath', ns.fdst_subpath): ns.entry,
            ('src', ns.fsrc2): ns.entry2,
            ('src_subpath', ns.fsrc_subpath2): ns.entry2,
            ('dst', ns.fdst2): ns.entry2,
            ('dst_subpath', ns.fdst_subpath2): ns.entry2,
        }

        self.assertEqual(expected, dict(map_))
        mocker.assert_not_called()

    @parametrize('quiet', (False, True))
    @mock.patch('jicheng.generate.log.warning')
    def test_add_entry_dupl_src(self, quiet, mocker):
        ns = self.general_test_case()
        ns = self.general_test_case(fsrc2=ns.fsrc)

        map_ = PathMap(ns.fsrc_root, ns.fdst_root)
        map_.add(ns.fsrc, ns.fdst)
        map_.add(ns.fsrc2, ns.fdst2, quiet=quiet)

        expected = {
            ('src', ns.fsrc): ns.entry,
            ('src_subpath', ns.fsrc_subpath): ns.entry,
            ('dst', ns.fdst): ns.entry,
            ('dst_subpath', ns.fdst_subpath): ns.entry,
        }

        self.assertEqual(expected, dict(map_))

        # check log warning called if not quiet mode
        if quiet:
            mocker.assert_not_called()
        else:
            mocker.assert_called_once()

    @parametrize('quiet', (False, True))
    @mock.patch('jicheng.generate.log.warning')
    def test_add_entry_dupl_dst(self, quiet, mocker):
        ns = self.general_test_case()
        ns = self.general_test_case(fdst2=ns.fdst)

        map_ = PathMap(ns.fsrc_root, ns.fdst_root)
        map_.add(ns.fsrc, ns.fdst)
        map_.add(ns.fsrc2, ns.fdst2, quiet=quiet)

        expected = {
            ('src', ns.fsrc): ns.entry,
            ('src_subpath', ns.fsrc_subpath): ns.entry,
            ('dst', ns.fdst): ns.entry,
            ('dst_subpath', ns.fdst_subpath): ns.entry,
        }

        self.assertEqual(expected, dict(map_))

        # check log warning called if not quiet mode
        if quiet:
            mocker.assert_not_called()
        else:
            mocker.assert_called_once()

    def test_get(self):
        ns = self.general_test_case()

        map_ = PathMap(ns.fsrc_root, ns.fdst_root)
        map_.add(ns.fsrc, ns.fdst)

        for type_, key, expected in (
            ('src', ns.fsrc, ns.entry),
            ('dst', ns.fdst, ns.entry),
            ('src_subpath', ns.fsrc_subpath, ns.entry),
            ('dst_subpath', ns.fdst_subpath, ns.entry),
            ('src', '!nonexist!', ns.entry_dummy),
            ('dst', '!nonexist!', ns.entry_dummy),
            ('src_subpath', '!nonexist!', ns.entry_dummy),
            ('dst_subpath', '!nonexist!', ns.entry_dummy),
        ):
            with self.subTest(type=type_, key=key, expected=expected):
                self.assertEqual(expected, map_.get(key, type_))


class TestCacheHandler(unittest.TestCase):
    def setUp(self):
        """Set up a temp directory for testing."""
        self.root = tempfile.mkdtemp(dir=tmpdir)
        self.fcache = os.path.join(self.root, 'cache.json')

    def test_init(self):
        """Basic"""
        # given
        cache_data = {
            'schema': 2,
            'src': {
                'key1': 'value1',
                'key2': 'value2',
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        cache = CacheHandler(self.fcache, self.root)

        # expect
        expected = cache_data

        self.assertEqual(expected, cache)
        self.assertEqual(self.root, cache.root)
        self.assertEqual(self.fcache, cache.file)
        self.assertFalse(cache.updated)

    def test_init_01(self):
        """Default if file not found."""
        # given
        # when
        cache = CacheHandler(self.fcache, self.root)

        # expect
        expected = {
            'schema': 2,
            'src': {},
        }
        self.assertEqual(expected, cache)
        self.assertEqual(self.root, cache.root)
        self.assertEqual(self.fcache, cache.file)
        self.assertFalse(cache.updated)

    def test_init_02(self):
        """Default if schema > 2."""
        # given
        cache_data = {
            'schema': 3,
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        cache = CacheHandler(self.fcache, self.root)

        # expect
        expected = {
            'schema': 2,
            'src': {},
        }
        self.assertEqual(expected, cache)
        self.assertEqual(self.root, cache.root)
        self.assertEqual(self.fcache, cache.file)
        self.assertFalse(cache.updated)

    def test_init_03(self):
        """Default if schema == 0."""
        # given
        cache_data = {
            'schema': 0,
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        cache = CacheHandler(self.fcache, self.root)

        # expect
        expected = {
            'schema': 2,
            'src': {},
        }
        self.assertEqual(expected, cache)
        self.assertEqual(self.root, cache.root)
        self.assertEqual(self.fcache, cache.file)
        self.assertFalse(cache.updated)

    def test_init_04(self):
        """Migrate if schema == 1."""
        # given
        cache_data = {
            'schema': 1,
            'assets': {
                'key1': 'value1',
                'key2': 'value2',
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        cache = CacheHandler(self.fcache, self.root)

        # expect
        expected = {
            'schema': 2,
            'src': {
                'key1': 'value1',
                'key2': 'value2',
            },
        }
        self.assertEqual(expected, cache)
        self.assertEqual(self.root, cache.root)
        self.assertEqual(self.fcache, cache.file)
        self.assertTrue(cache.updated)

    def test_update_file_stat_unchanged(self):
        # given
        file = os.path.join(self.root, 'file.txt')
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abc')

        cache_data = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        sleep(1E-6)
        cache = CacheHandler(self.fcache, self.root)
        cache.update_file_stat(file)

        # expect
        expected_cache = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        expected_updated_files = {
            'file.txt': False,
        }
        self.assertEqual(expected_cache, cache)
        self.assertEqual(expected_updated_files, cache.updated_files)
        self.assertFalse(cache.updated)

    def test_update_file_stat_content_change(self):
        # given
        file = os.path.join(self.root, 'file.txt')
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abc')

        cache_data = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        sleep(1E-6)
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('def')
        cache = CacheHandler(self.fcache, self.root)
        cache.update_file_stat(file)

        # expect
        expected_cache = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        expected_updated_files = {
            'file.txt': True,
        }
        self.assertEqual(expected_cache, cache)
        self.assertEqual(expected_updated_files, cache.updated_files)
        self.assertTrue(cache.updated)

    def test_update_file_stat_size_change(self):
        # given
        file = os.path.join(self.root, 'file.txt')
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abc')

        cache_data = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        sleep(1E-6)
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abcd')
        cache = CacheHandler(self.fcache, self.root)
        cache.update_file_stat(file)

        # expect
        expected_cache = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        expected_updated_files = {
            'file.txt': True,
        }
        self.assertEqual(expected_cache, cache)
        self.assertEqual(expected_updated_files, cache.updated_files)
        self.assertTrue(cache.updated)

    def test_update_file_stat_mtime_change(self):
        # given
        file = os.path.join(self.root, 'file.txt')
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abc')

        cache_data = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        sleep(1E-6)
        os.utime(file)
        cache = CacheHandler(self.fcache, self.root)
        cache.update_file_stat(file)

        # expect
        expected_cache = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        expected_updated_files = {
            'file.txt': False,
        }
        self.assertEqual(expected_cache, cache)
        self.assertEqual(expected_updated_files, cache.updated_files)
        self.assertTrue(cache.updated)

    def test_update_file_stat_deleted(self):
        # given
        file = os.path.join(self.root, 'file.txt')
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abc')

        cache_data = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        sleep(1E-6)
        os.remove(file)
        cache = CacheHandler(self.fcache, self.root)
        cache.update_file_stat(file)

        # expect
        expected_cache = {
            'schema': 2,
            'src': {},
        }
        expected_updated_files = {
            'file.txt': True,
        }
        self.assertEqual(expected_cache, cache)
        self.assertEqual(expected_updated_files, cache.updated_files)
        self.assertTrue(cache.updated)

    def test_update_file_stat_new_file(self):
        # given
        file = os.path.join(self.root, 'file.txt')
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abc')

        cache_data = {
            'schema': 2,
            'src': {},
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        cache = CacheHandler(self.fcache, self.root)
        cache.update_file_stat(file)

        # expect
        expected_cache = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        expected_updated_files = {
            'file.txt': True,
        }
        self.assertEqual(expected_cache, cache)
        self.assertEqual(expected_updated_files, cache.updated_files)
        self.assertTrue(cache.updated)

    def test_update_file_stat_new_nonexist(self):
        # given
        file = os.path.join(self.root, 'file.txt')

        cache_data = {
            'schema': 2,
            'src': {},
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))

        # when
        cache = CacheHandler(self.fcache, self.root)
        cache.update_file_stat(file)

        # expect
        expected_cache = {
            'schema': 2,
            'src': {},
        }
        expected_updated_files = {
            'file.txt': False,
        }
        self.assertEqual(expected_cache, cache)
        self.assertEqual(expected_updated_files, cache.updated_files)
        self.assertFalse(cache.updated)

    def test_save_updated(self):
        """Save if cache updated."""
        # given
        file = os.path.join(self.root, 'file.txt')
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abc')

        cache_data = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))
        old_mtime = os.path.getmtime(self.fcache)

        # when
        sleep(1E-6)
        os.utime(file)
        cache = CacheHandler(self.fcache, self.root)
        cache.update_file_stat(file)
        cache.save()

        # expect
        self.assertTrue(cache.updated)
        self.assertLess(old_mtime, os.path.getmtime(self.fcache))

    def test_save_not_updated(self):
        """Don't save if cache not updated."""
        # given
        file = os.path.join(self.root, 'file.txt')
        with open(file, 'w', encoding='UTF-8') as fh:
            fh.write('abc')

        cache_data = {
            'schema': 2,
            'src': {
                'file.txt': {
                    'hash': fchecksum(file),
                    'mtime': os.path.getmtime(file),
                    'size': os.path.getsize(file),
                },
            },
        }
        with open(self.fcache, 'w', encoding='UTF-8') as fh:
            fh.write(json.dumps(cache_data))
        old_mtime = os.path.getmtime(self.fcache)

        # when
        sleep(1E-6)
        cache = CacheHandler(self.fcache, self.root)
        cache.save()

        # expect
        self.assertFalse(cache.updated)
        self.assertEqual(old_mtime, os.path.getmtime(self.fcache))


class TestStaticSiteGenerator(unittest.TestCase):
    def setUp(self):
        """Set up data files for testing."""
        self.root = tempfile.mkdtemp(dir=tmpdir)

    def test_init(self):
        """Test default values if config not set."""
        # given
        gen = StaticSiteGenerator(self.root)

        # expect
        self.assertEqual('index', gen.index)
        self.assertEqual('html', gen.output_type)
        self.assertEqual('default', gen.theme)
        self.assertEqual({}, gen.assets)

        self.assertEqual(self.root, gen.dir_data)
        self.assertEqual(os.path.join(self.root, 'pages'), gen.dir_source)
        self.assertEqual(os.path.join(self.root, 'public'), gen.dir_public)
        self.assertEqual(os.path.join(self.root, 'themes', 'default'), gen.dir_theme)
        self.assertEqual(os.path.join(self.root, 'themes', 'default', 'static'), gen.dir_static)
        self.assertEqual(os.path.join(self.root, 'themes', 'default', 'template', 'html'), gen.dir_template)
        self.assertEqual(os.path.join(self.root, 'cache.json'), gen.file_cache)

    def test_init_01(self):
        """Take config value if set."""
        # given
        conf = {
            'index_filename': 'start',
            'output_format': 'html',
            'theme': 'mytheme',
            'source_dir': 'src/pages',
            'public_dir': 'dist/publish',
            'themes_dir': 'src/themes',
            'cache_file': '.cache',
            'assets': {
                'key1': 'value1',
                'key2': 'value2',
            },
            'theme_assets': {
                'mytheme': {
                    'key2': 'value2-2',
                    'key3': 'value3-2',
                },
            },
        }
        gen = StaticSiteGenerator(self.root, conf)

        # expect
        expected_assets = {
            'key1': 'value1',
            'key2': 'value2-2',
            'key3': 'value3-2',
        }
        self.assertEqual('start', gen.index)
        self.assertEqual('html', gen.output_type)
        self.assertEqual('mytheme', gen.theme)
        self.assertEqual(expected_assets, gen.assets)

        self.assertEqual(self.root, gen.dir_data)
        self.assertEqual(os.path.join(self.root, 'src', 'pages'), gen.dir_source)
        self.assertEqual(os.path.join(self.root, 'dist', 'publish'), gen.dir_public)
        self.assertEqual(os.path.join(self.root, 'src', 'themes', 'mytheme'), gen.dir_theme)
        self.assertEqual(os.path.join(self.root, 'src', 'themes', 'mytheme', 'static'), gen.dir_static)
        self.assertEqual(os.path.join(self.root, 'src', 'themes', 'mytheme', 'template', 'html'), gen.dir_template)
        self.assertEqual(os.path.join(self.root, '.cache'), gen.file_cache)

    @mock.patch('jicheng.generate.os.remove')
    @mock.patch('jicheng.generate.shutil.rmtree')
    @mock.patch('jicheng.generate.log')
    def test_clean(self, _, m_rmtree, m_remove):
        """Test if removing is called upon public dir and cache file."""
        gen = StaticSiteGenerator(self.root)
        gen.clean()
        m_rmtree.assert_called_once_with(gen.dir_public)
        m_remove.assert_called_once_with(gen.file_cache)

    @mock.patch('jicheng.generate.log')
    def test_run(self, _):
        """Test if related handlers are called."""
        with mock.patch('jicheng.generate.os.makedirs') as m_makedirs, \
             mock.patch('jicheng.generate.Parser') as m_parser, \
             mock.patch('jicheng.generate.TemplateHandler') as m_tpl, \
             mock.patch('jicheng.generate.CacheHandler') as m_cache, \
             mock.patch('jicheng.generate.StaticSiteGenerator.map_src_and_dst'), \
             mock.patch('jicheng.generate.StaticSiteGenerator.copy_public'), \
             mock.patch('jicheng.generate.StaticSiteGenerator.generate_html'), \
             mock.patch('jicheng.generate.StaticSiteGenerator.remove_stale_files'), \
             mock.patch('jicheng.generate.StaticSiteGenerator.remove_stale_cache'), \
             mock.patch('jicheng.generate.StaticSiteGenerator.generate_html'):
            gen = StaticSiteGenerator(self.root)
            gen.run()

        m_makedirs.assert_called_once_with(gen.dir_public, exist_ok=True)
        m_parser.assert_called_once_with(char_subst_table=None, renderer_filter=None)
        m_tpl.assert_called_once_with(gen.dir_template, gen)
        m_cache.assert_called_once_with(gen.file_cache, gen.dir_data)
