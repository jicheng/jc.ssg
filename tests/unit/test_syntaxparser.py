import io
import unittest
from contextlib import nullcontext
from textwrap import dedent
from unittest import mock

import lxml.html

from jicheng.syntaxparser import Call as LexerCall
from jicheng.syntaxparser import (
    Lexer,
    LexerHtml,
    Parser,
    Renderer,
    RendererHtml,
)

from .. import parametrize


class TestParser(unittest.TestCase):
    def test_to_camelcase(self):
        with self.subTest("""Basic"""):
            self.assertEqual('snakeCase', Parser.to_camelcase('snake_case'))
            self.assertEqual('snakeCase', Parser.to_camelcase('snake__case'))
            self.assertEqual('snakeCase', Parser.to_camelcase('snake_Case'))
            self.assertEqual('SnakeCase', Parser.to_camelcase('Snake_Case'))
        with self.subTest("""Alternative delim"""):
            self.assertEqual('kebabCase', Parser.to_camelcase('kebab-case', delim='-'))

    def test_to_uppercamelcase(self):
        with self.subTest("""Basic"""):
            self.assertEqual('SnakeCase', Parser.to_uppercamelcase('snake_case'))
            self.assertEqual('SnakeCase', Parser.to_uppercamelcase('snake__case'))
            self.assertEqual('SnakeCase', Parser.to_uppercamelcase('snake_Case'))
            self.assertEqual('SnakeCase', Parser.to_uppercamelcase('Snake_Case'))
        with self.subTest("""Alternative delim"""):
            self.assertEqual('KebabCase', Parser.to_uppercamelcase('kebab-case', delim='-'))

    def test_get_lexer(self):
        with self.subTest("""Basic"""):
            self.assertEqual(Lexer, Parser.get_lexer(''))
            self.assertEqual(LexerHtml, Parser.get_lexer('html'))
        with self.subTest("""Nonexist"""):
            self.assertEqual(None, Parser.get_lexer('!nonexist!'))
        with self.subTest("""Bad type"""):
            with mock.patch('jicheng.syntaxparser.LexerDummy', 123, create=True):
                self.assertEqual(None, Parser.get_lexer('dummy'))

    def test_get_renderer(self):
        with self.subTest("""Basic"""):
            self.assertEqual(Renderer, Parser.get_renderer(''))
            self.assertEqual(RendererHtml, Parser.get_renderer('html'))
        with self.subTest("""Nonexist"""):
            self.assertEqual(None, Parser.get_renderer('!nonexist!'))
        with self.subTest("""Bad type"""):
            with mock.patch('jicheng.syntaxparser.RendererDummy', 123, create=True):
                self.assertEqual(None, Parser.get_renderer('dummy'))

    def test_run(self):
        """Test if corresponding lexer and renderer are called."""
        sentinel_char_subst_table = {
            '版': {'古': '板', '今': '版'},
        }
        sentinel_renderer_filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide'},
        ]
        sentinel_lexer_info = {'sentinel': 'lexer_info'}
        sentinel_lexer_calls = ['sentinel', 'lexer', 'calls']
        sentinel_renderer_output = 'sentinel_renderer_output'

        with mock.patch('jicheng.syntaxparser.Parser.get_lexer') as m_lexer, \
             mock.patch('jicheng.syntaxparser.Parser.get_renderer') as m_renderer:
            m_lexer.return_value.return_value.info = sentinel_lexer_info
            m_lexer.return_value.return_value.calls = sentinel_lexer_calls
            m_renderer.return_value.return_value.render.return_value = sentinel_renderer_output

            parser = Parser(
                char_subst_table=sentinel_char_subst_table,
                renderer_filter=sentinel_renderer_filter,
            )
            with io.BytesIO(b'dummy_html') as fh:
                output = parser.run(fh, imode='html', omode='html')

        # test if we get correct info and output
        self.assertEqual(sentinel_lexer_info, parser.info)
        self.assertEqual(sentinel_renderer_output, parser.output)
        self.assertEqual(sentinel_renderer_output, output)

        # test if the correct lexer and renderer are called with expected args
        m_lexer.assert_has_calls([
            mock.call('html'),
            mock.call()(char_subst_table=sentinel_char_subst_table),
            mock.call()().parse(fh),
        ])
        m_renderer.assert_has_calls([
            mock.call('html'),
            mock.call()(filter=sentinel_renderer_filter),
            mock.call()().render(sentinel_lexer_calls),
        ])

    @parametrize('omode', (None, '', False))
    def test_run_no_render(self, omode):
        """Test if rendering is skipped if omode is falsy."""
        sentinel_lexer_info = {'sentinel': 'lexer_info'}

        with mock.patch('jicheng.syntaxparser.Parser.get_lexer') as m_lexer, \
             mock.patch('jicheng.syntaxparser.Parser.get_renderer') as m_renderer, \
             mock.patch('jicheng.syntaxparser.RendererHtml') as m_renderer2:
            m_lexer.return_value.return_value.info = sentinel_lexer_info

            parser = Parser()
            with io.BytesIO(b'dummy_html') as fh:
                output = parser.run(fh, imode='html', omode=omode)

        # test if we get correct info and output
        self.assertEqual(sentinel_lexer_info, parser.info)
        self.assertIsNone(parser.output)
        self.assertIsNone(output)

        # test if the correct lexer is called with expected args
        m_lexer.assert_has_calls([
            mock.call('html'),
            mock.call()(char_subst_table=None),
            mock.call()().parse(fh),
        ])

        # test if renderer is not called
        m_renderer.assert_not_called()
        m_renderer2.assert_not_called()

    def test_run_cache(self):
        """Test if initial args are cached and passed among multiple runs."""
        sentinel_lexer_calls = ['sentinel', 'lexer', 'calls']

        with mock.patch('jicheng.syntaxparser.Parser.get_lexer') as m_lexer, \
             mock.patch('jicheng.syntaxparser.Parser.get_renderer') as m_renderer:
            m_lexer.return_value.return_value.calls = sentinel_lexer_calls

            parser = Parser(
                char_subst_table={
                    '版': {'古': '板', '今': '版'},
                },
                renderer_filter=[
                    {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide'},
                ],
            )

            char_subst_table = parser.char_subst_table
            renderer_filter = parser.renderer_filter

            with io.BytesIO(b'dummy_html') as fh:
                parser.run(fh, imode='html', omode='html')

            with io.BytesIO(b'dummy_html2') as fh2:
                parser.run(fh2, imode='txt', omode='txt')

        m_lexer.assert_has_calls([
            mock.call('html'),
            mock.call()(char_subst_table=char_subst_table),
            mock.call()().parse(fh),
            mock.call('txt'),
            mock.call()(char_subst_table=char_subst_table),
            mock.call()().parse(fh2),
        ])

        # test if they are the same object
        self.assertIs(char_subst_table, m_lexer.mock_calls[1].kwargs['char_subst_table'])
        self.assertIs(char_subst_table, m_lexer.mock_calls[4].kwargs['char_subst_table'])

        m_renderer.assert_has_calls([
            mock.call('html'),
            mock.call()(filter=renderer_filter),
            mock.call()().render(sentinel_lexer_calls),
            mock.call('txt'),
            mock.call()(filter=renderer_filter),
            mock.call()().render(sentinel_lexer_calls),
        ])

        # test if they are the same object
        self.assertIs(renderer_filter, m_renderer.mock_calls[1].kwargs['filter'])
        self.assertIs(renderer_filter, m_renderer.mock_calls[4].kwargs['filter'])


class TestLexerHtml(unittest.TestCase):
    def _test_parse_dl(self, html, expected):
        def fn(x):
            return lxml.html.etree.tostring(x, encoding='unicode')

        result = LexerHtml.parse_dl(lxml.html.fromstring(html))
        result = [{k: [fn(vv) for vv in v] for k, v in g.items()} for g in result]
        self.assertEqual(expected, result)

    def test_parse_dl(self):
        html = dedent(
            """\
            <dl>
            <dt>key1</dt>
            <dd>value1</dd>
            <dt>key2-1</dt>
            <dt>key2-2</dt>
            <dt>key2-3</dt>
            <dd>value2</dd>
            <dt>key3</dt>
            <dd>value3-1</dd>
            <dd>value3-2</dd>
            <dd>value3-3</dd>
            <dt>key4-1</dt>
            <dt>key4-2</dt>
            <dd>value4-1</dd>
            <dd>value4-2</dd>
            </dl>
            """
        )
        expected = [
            {'name': ['<dt>key1</dt>\n'], 'value': ['<dd>value1</dd>\n']},
            {'name': ['<dt>key2-1</dt>\n', '<dt>key2-2</dt>\n', '<dt>key2-3</dt>\n'], 'value': ['<dd>value2</dd>\n']},
            {'name': ['<dt>key3</dt>\n'], 'value': ['<dd>value3-1</dd>\n', '<dd>value3-2</dd>\n', '<dd>value3-3</dd>\n']},
            {'name': ['<dt>key4-1</dt>\n', '<dt>key4-2</dt>\n'], 'value': ['<dd>value4-1</dd>\n', '<dd>value4-2</dd>\n']},
        ]
        self._test_parse_dl(html, expected)

    def test_parse_dl_div_good(self):
        html = dedent(
            """\
            <dl>
            <div>
            <dt>key1</dt>
            <dd>value1</dd>
            </div>
            <div>
            <dt>key2-1</dt>
            <dt>key2-2</dt>
            <dt>key2-3</dt>
            <dd>value2</dd>
            </div>
            <div>
            <dt>key3</dt>
            <dd>value3-1</dd>
            <dd>value3-2</dd>
            <dd>value3-3</dd>
            </div>
            <div>
            <dt>key4-1</dt>
            <dt>key4-2</dt>
            <dd>value4-1</dd>
            <dd>value4-2</dd>
            </div>
            </dl>
            """
        )
        expected = [
            {'name': ['<dt>key1</dt>\n'], 'value': ['<dd>value1</dd>\n']},
            {'name': ['<dt>key2-1</dt>\n', '<dt>key2-2</dt>\n', '<dt>key2-3</dt>\n'], 'value': ['<dd>value2</dd>\n']},
            {'name': ['<dt>key3</dt>\n'], 'value': ['<dd>value3-1</dd>\n', '<dd>value3-2</dd>\n', '<dd>value3-3</dd>\n']},
            {'name': ['<dt>key4-1</dt>\n', '<dt>key4-2</dt>\n'], 'value': ['<dd>value4-1</dd>\n', '<dd>value4-2</dd>\n']},
        ]
        self._test_parse_dl(html, expected)

    def test_parse_dl_div_bad_01(self):
        """Bad div: key-value pair crossing div."""
        html = dedent(
            """\
            <dl>
            <div><dt>key1</dt></div>
            <div><dd>value1</dd></div>
            </dl>
            """
        )
        expected = [
            {'name': ['<dt>key1</dt>'], 'value': ['<dd>value1</dd>']},
        ]
        self._test_parse_dl(html, expected)

    def test_parse_dl_div_bad_02(self):
        """Bad div: key-value pair without div"""
        html = dedent(
            """\
            <dl><div>
            <dt>key1</dt>
            <dd>value1</dd>
            <dt>key2</dt>
            <dd>value2</dd>
            </div></dl>
            """
        )
        expected = [
            {'name': ['<dt>key1</dt>\n'], 'value': ['<dd>value1</dd>\n']},
            {'name': ['<dt>key2</dt>\n'], 'value': ['<dd>value2</dd>\n']},
        ]
        self._test_parse_dl(html, expected)

    def _test_load_dl_key_value(self, html, expected):
        output = LexerHtml.load_dl_key_value(lxml.html.fromstring(html))
        self.assertEqual(expected, output)

    def test_load_dl_key_value(self):
        html = dedent(
            """\
            <dl>
            <dt>key1</dt>
            <dd>value1</dd>
            <dt>key2-1</dt>
            <dt>key2-2</dt>
            <dt>key2-3</dt>
            <dd>value2</dd>
            <dt>key3</dt>
            <dd>value3-1</dd>
            <dd>value3-2</dd>
            <dd>value3-3</dd>
            <dt>key4-1</dt>
            <dt>key4-2</dt>
            <dd>value4-1</dd>
            <dd>value4-2</dd>
            </dl>
            """
        )
        expected = {
            'key1': ['value1'],
            'key2-1': ['value2'],
            'key2-2': ['value2'],
            'key2-3': ['value2'],
            'key3': ['value3-1', 'value3-2', 'value3-3'],
            'key4-1': ['value4-1', 'value4-2'],
            'key4-2': ['value4-1', 'value4-2'],
        }
        self._test_load_dl_key_value(html, expected)

    def test_load_dl_key_value_data(self):
        """Load from data[value]."""
        html = dedent(
            """\
            <dl>
            <dt>key1</dt>
            <dd>text1 <data value="value1">text2</data> <data value="value2">text3</data> text4</dd>
            </dl>
            """
        )
        expected = {
            'key1': ['value1', 'value2'],
        }
        self._test_load_dl_key_value(html, expected)

    def _test_parse_info(self, html, expected, expect_raise=False):
        """Test whether the parsed info matches the expected

        Args:
            expected (dict): a dict defining (key, value) pairs to match
                - A key with NotImplemented value is asserted not to exist
                - Non-listed (key, value) are not checked
        """
        lexer = LexerHtml()
        with io.BytesIO(html.encode('UTF-8')) as fh:
            lexer.parse(fh)
        _actual = {k: lexer.info[k] for k, v in expected.items() if k in lexer.info}
        _expected = {k: v for k, v in expected.items() if v is not NotImplemented}
        with self.assertRaises(AssertionError) if expect_raise else nullcontext():
            self.assertEqual(_expected, _actual)

    def test_parse_info_empty(self):
        html = """<p>dummy text</p>"""
        expected = {}
        self._test_parse_info(html, expected)

    def test_parse_info_data_type(self):
        """Take first header[data-type]."""
        html = dedent(
            """\
            <header data-type="book"></header>
            <header data-type="book1"></header>
            <p>dummy text</p>
            """
        )
        expected = {'data-type': 'book'}
        self._test_parse_info(html, expected)

    def test_parse_info_title(self):
        """Invalid h#."""
        html = dedent(
            """\
            <h1>我的標題1</h1>
            <h2>我的標題2</h2>
            """
        )
        expected = {}
        self._test_parse_info(html, expected)

    def test_parse_info_title_first_header_h1(self):
        """Take first header > h1."""
        html = dedent(
            """\
            <h1>我的標題1</h1>
            <header>
            <h2>我的標題2</h2>
            <h1>我的標題</h1>
            <h1>我的標題3</h1>
            </header>
            <header>
            <h1>我的標題4</h1>
            </header>
            <h1>我的標題5</h1>
            """
        )
        expected = {'title': '我的標題'}
        self._test_parse_info(html, expected)

    def _test_parse_info_book(self, html_tpl, expected, data_type='book', cls='元資料', dt='', book_only=True):
        """Test metadata parsing for data-type="book"."""
        header_attr = f' data-type="{data_type}"' if data_type is not None else ''
        dl_attr = f' class="foo {cls} bar"' if cls is not None else ''
        expect_raise = book_only and not (data_type == 'book' and cls == '元資料')

        html = html_tpl.format(
            dt=dt,
            header_attr=header_attr,
            dl_attr=dl_attr,
        )

        with self.subTest(html=html):
            self._test_parse_info(
                html,
                expected,
                expect_raise,
            )

    @parametrize('data-type', ('book', 'other', None))
    @parametrize('class', ('元資料', 'other', None))
    @parametrize('dt', ('標題', '書名', '篇名', '名稱'))
    def test_parse_info_book_title(self, dt, cls, data_type):
        html_tpl = dedent(
            """\
            <header{header_attr}>
            <dl{dl_attr}>
            <div><dt>{dt}<dd>元資料標題</div>
            </dl>
            </header>
            """
        )
        expected = {'title': '元資料標題'}
        self._test_parse_info_book(html_tpl, expected, data_type=data_type, cls=cls, dt=dt)

    @parametrize('data-type', ('book', 'other', None))
    @parametrize('class', ('元資料', 'other', None))
    @parametrize('dt', ('標題', '書名', '篇名', '名稱'))
    def test_parse_info_book_title_01(self, dt, cls, data_type):
        """Take header > h1 in prior."""
        html_tpl = dedent(
            """\
            <header{header_attr}>
            <h1>頁面標題</h1>
            <dl{dl_attr}>
            <div><dt>{dt}<dd>元資料標題</div>
            </dl>
            </header>
            """
        )
        expected = {'title': '頁面標題'}
        self._test_parse_info_book(html_tpl, expected, data_type=data_type, cls=cls, dt=dt, book_only=False)

    @parametrize('data-type', ('book', 'other', None))
    @parametrize('class', ('元資料', 'other', None))
    @parametrize('dt', ('標題', '書名', '篇名', '名稱'))
    def test_parse_info_book_title_02(self, dt, cls, data_type):
        """Take header > h1 in prior (even if appear later)."""
        html_tpl = dedent(
            """\
            <header{header_attr}>
            <dl{dl_attr}>
            <div><dt>{dt}<dd>元資料標題</div>
            </dl>
            <h1>頁面標題</h1>
            </header>
            """
        )
        expected = {'title': '頁面標題'}
        self._test_parse_info_book(html_tpl, expected, data_type=data_type, cls=cls, dt=dt, book_only=False)

    @parametrize('data-type', ('book', 'other', None))
    @parametrize('class', ('元資料', 'other', None))
    def test_parse_info_book_ancient(self, cls, data_type):
        html_tpl = dedent(
            """\
            <header{header_attr}>
            <dl{dl_attr}>
            <div><dt>版式<dd>古版</div>
            </dl>
            </header>
            """
        )
        expected = {'版式': '古版'}
        self._test_parse_info_book(html_tpl, expected, data_type=data_type, cls=cls)

    @parametrize('data-type', ('book', 'other', None))
    @parametrize('class', ('元資料', 'other', None))
    def test_parse_info_book_char_subst(self, cls, data_type):
        html_tpl = dedent(
            """\
            <header{header_attr}>
            <dl{dl_attr}>
            <div><dt>字元替換<dd>{{"爲": {{"古": "爲", "今": "為"}}}}</div>
            </dl>
            </header>
            """
        )
        expected = {'字元替換': {'爲': {'今': '為', '古': '爲'}}}
        self._test_parse_info_book(html_tpl, expected, data_type=data_type, cls=cls)

    @parametrize('data-type', ('book', 'other', None))
    @parametrize('class', ('元資料', 'other', None))
    def test_parse_info_book_char_subst_merge(self, cls, data_type):
        """Merge duplicates."""
        html_tpl = dedent(
            """\
            <header{header_attr}>
            <dl{dl_attr}>
            <div><dt>字元替換<dd>{{"爲": {{"古": "爲", "今": "為"}}, "温": {{"古": "温", "今": "溫"}}}}</div>
            <div><dt>字元替換<dd>{{"爲": {{"古": "爲", "今": "为"}}, "温": null}}</div>
            </dl>
            </header>
            """
        )
        expected = {'字元替換': {'爲': {'今': '为', '古': '爲'}, '温': None}}
        self._test_parse_info_book(html_tpl, expected, data_type=data_type, cls=cls)

    def _test_parse_calls(self, html, expected, expect_raise=False):
        """Test whether html is parsed as expected."""
        lexer = LexerHtml()
        with io.BytesIO(html.encode('UTF-8')) as fh:
            lexer.parse(fh)
        with self.assertRaises(AssertionError) if expect_raise else nullcontext():
            self.assertEqual(expected, lexer.calls)

    def test_parse_calls_ignore_html_body(self):
        """Ignore html, body."""
        html = dedent(
            """\
            <!DOCTYPE html>
            <html>
            <body>
            <p>text</p>
            </body>
            </html>"""
        )
        expected = [
            LexerCall(command='doc_start', data={}),
            LexerCall(command='html_start_tag', data={'tag': 'p', 'attrs': {}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='html_end_tag', data={'tag': 'p', 'attrs': {}}),
            LexerCall(command='cdata', data={'text': '\n'}),
            LexerCall(command='doc_end', data={}),
        ]
        self._test_parse_calls(html, expected)

    @parametrize('tag', ('h1', 'h2', 'h3', 'h4', 'h5', 'h6'))
    def test_parse_calls_hx(self, tag):
        """Add data-sec=h# for h#."""
        html = f"""<{tag}>text</{tag}>"""
        expected = [
            LexerCall(command='doc_start', data={}),
            LexerCall(command='html_start_tag', data={'tag': tag, 'attrs': {'data-sec': tag}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='html_end_tag', data={'tag': tag, 'attrs': {'data-sec': tag}}),
            LexerCall(command='doc_end', data={}),
        ]
        self._test_parse_calls(html, expected)

    @parametrize('tag', ('h1', 'h2', 'h3', 'h4', 'h5', 'h6'))
    def test_parse_calls_hx_except_body_header(self, tag):
        """Don't add data-sec=h# for h# if in body > header."""
        html = f"""<header><{tag}>text</{tag}></header>"""
        expected = [
            LexerCall(command='doc_start', data={'title': 'text'} if tag == 'h1' else {}),
            LexerCall(command='html_start_tag', data={'tag': 'header', 'attrs': {}}),
            LexerCall(command='html_start_tag', data={'tag': tag, 'attrs': {}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='html_end_tag', data={'tag': tag, 'attrs': {}}),
            LexerCall(command='html_end_tag', data={'tag': 'header', 'attrs': {}}),
            LexerCall(command='doc_end', data={'title': 'text'} if tag == 'h1' else {}),
        ]
        self._test_parse_calls(html, expected)

    @parametrize('tag', ('h1', 'h2', 'h3', 'h4', 'h5', 'h6'))
    def test_parse_calls_hx_other_header(self, tag):
        """Add data-sec=h# for h# if in another header."""
        html = f"""<div><header><{tag}>text</{tag}></header></div>"""
        expected = [
            LexerCall(command='doc_start', data={}),
            LexerCall(command='html_start_tag', data={'tag': 'div', 'attrs': {}}),
            LexerCall(command='html_start_tag', data={'tag': 'header', 'attrs': {}}),
            LexerCall(command='html_start_tag', data={'tag': tag, 'attrs': {'data-sec': tag}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='html_end_tag', data={'tag': tag, 'attrs': {'data-sec': tag}}),
            LexerCall(command='html_end_tag', data={'tag': 'header', 'attrs': {}}),
            LexerCall(command='html_end_tag', data={'tag': 'div', 'attrs': {}}),
            LexerCall(command='doc_end', data={}),
        ]
        self._test_parse_calls(html, expected)

    def _test_parse_calls_book(self, html, expected, data_type, book_only=True):
        """Test for data-type="book".

        Automatically add related <header> element and common calls.
        """
        attr = f' data-type="{data_type}"' if data_type else ''
        doc_data = {'data-type': data_type} if data_type else {}
        header_data = {'tag': 'header', 'attrs': {'data-type': data_type} if data_type else {}}
        expect_raise = book_only and (data_type != 'book')

        html = f"""<header{attr}></header>""" + html
        expected = ([
            LexerCall(command='doc_start', data=doc_data),
            LexerCall(command='html_start_tag', data=header_data),
            LexerCall(command='html_end_tag', data=header_data),
        ] + expected + [
            LexerCall(command='doc_end', data=doc_data),
        ])

        self._test_parse_calls(html, expected, expect_raise)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_p(self, data_type):
        html = """<p>text</p>"""
        expected = [
            LexerCall(command='html_start_tag', data={'tag': 'div', 'attrs': {'data-sec': 'p'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='html_end_tag', data={'tag': 'div', 'attrs': {'data-sec': 'p'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_ins(self, data_type):
        html = """<ins>text</ins>"""
        expected = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '今版'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '今版'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_del(self, data_type):
        html = """<del>text</del>"""
        expected = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_marginalnote(self, data_type):
        html = """<aside class="foo 眉批 bar">text</aside>"""
        expected = [
            LexerCall(command='marginalnote_start', data={'attrs': {'class': 'foo 眉批 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='marginalnote_end', data={'attrs': {'class': 'foo 眉批 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_parallel_note(self, data_type):
        html = """<small class="foo 組排小字 bar">text</small>"""
        expected = [
            LexerCall(command='parallel_note_start', data={'attrs': {'class': 'foo 組排小字 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='parallel_note_end', data={'attrs': {'class': 'foo 組排小字 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_parallel_note_01(self, data_type):
        """With (span.右文, span.左文)."""
        html = (
            """<small class="foo 組排小字 bar">"""
            """<span class="foo 右文 bar">textR</span>"""
            """<span class="foo 左文 bar">textL</span>"""
            """</small>"""
        )
        expected = [
            LexerCall(command='parallel_note_start', data={'attrs': {'class': 'foo 組排小字 bar'}}),
            LexerCall(command='parallel_note_right_start', data={'attrs': {'class': 'foo 右文 bar'}}),
            LexerCall(command='cdata', data={'text': 'textR'}),
            LexerCall(command='parallel_note_right_end', data={'attrs': {'class': 'foo 右文 bar'}}),
            LexerCall(command='parallel_note_left_start', data={'attrs': {'class': 'foo 左文 bar'}}),
            LexerCall(command='cdata', data={'text': 'textL'}),
            LexerCall(command='parallel_note_left_end', data={'attrs': {'class': 'foo 左文 bar'}}),
            LexerCall(command='parallel_note_end', data={'attrs': {'class': 'foo 組排小字 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_parallel_note_02(self, data_type):
        """Don't convert other (span.右文, span.左文)."""
        html = (
            """<span class="foo 右文 bar">textR</span>"""
            """<span class="foo 左文 bar">textL</span>"""
        )
        expected = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'class': 'foo 右文 bar'}}),
            LexerCall(command='cdata', data={'text': 'textR'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'class': 'foo 右文 bar'}}),
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'class': 'foo 左文 bar'}}),
            LexerCall(command='cdata', data={'text': 'textL'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'class': 'foo 左文 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type, book_only=False)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_cutting_note(self, data_type):
        html = """<small class="foo 雙行夾注 bar">text</small>"""
        expected = [
            LexerCall(command='cutting_note_start', data={'attrs': {'class': 'foo 雙行夾注 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='cutting_note_end', data={'attrs': {'class': 'foo 雙行夾注 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_footnote(self, data_type):
        html = """<small class="foo 腳注 bar">text</small>"""
        expected = [
            LexerCall(command='footnote_start', data={'attrs': {'class': 'foo 腳注 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='footnote_end', data={'attrs': {'class': 'foo 腳注 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_rubynote(self, data_type):
        html = """<small class="foo 旁注 bar">text</small>"""
        expected = [
            LexerCall(command='rubynote_start', data={'attrs': {'class': 'foo 旁注 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='rubynote_end', data={'attrs': {'class': 'foo 旁注 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_whiteonblack(self, data_type):
        html = """<b class="foo 陰文 bar">text</b>"""
        expected = [
            LexerCall(command='whiteonblack_start', data={'attrs': {'class': 'foo 陰文 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='whiteonblack_end', data={'attrs': {'class': 'foo 陰文 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_round_whiteonblack(self, data_type):
        html = """<b class="foo 圓角陰文 bar">text</b>"""
        expected = [
            LexerCall(command='round_whiteonblack_start', data={'attrs': {'class': 'foo 圓角陰文 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='round_whiteonblack_end', data={'attrs': {'class': 'foo 圓角陰文 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_bordered(self, data_type):
        html = """<b class="foo 方外框 bar">text</b>"""
        expected = [
            LexerCall(command='bordered_start', data={'attrs': {'class': 'foo 方外框 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='bordered_end', data={'attrs': {'class': 'foo 方外框 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_round_bordered(self, data_type):
        html = """<b class="foo 圓外框 bar">text</b>"""
        expected = [
            LexerCall(command='round_bordered_start', data={'attrs': {'class': 'foo 圓外框 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='round_bordered_end', data={'attrs': {'class': 'foo 圓外框 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_circled(self, data_type):
        html = """<b class="foo 圓圈 bar">text</b>"""
        expected = [
            LexerCall(command='circled_start', data={'attrs': {'class': 'foo 圓圈 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='circled_end', data={'attrs': {'class': 'foo 圓圈 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)

    @parametrize('data-type', ('book', 'other', None))
    def test_parse_calls_book_parenthesized(self, data_type):
        html = """<b class="foo 圓括號 bar">text</b>"""
        expected = [
            LexerCall(command='parenthesized_start', data={'attrs': {'class': 'foo 圓括號 bar'}}),
            LexerCall(command='cdata', data={'text': 'text'}),
            LexerCall(command='parenthesized_end', data={'attrs': {'class': 'foo 圓括號 bar'}}),
        ]
        self._test_parse_calls_book(html, expected, data_type=data_type)


class TestRenderer(unittest.TestCase):
    def _test_render(self, calls, expected):
        renderer = Renderer()
        self.assertEqual(expected, renderer.render(calls))

    def test_render_cdata(self):
        calls = [
            LexerCall(command='cdata', data={'text': '一段<br><font color="red">文字</font>'}),
        ]
        expected = """一段<br><font color="red">文字</font>"""
        self._test_render(calls, expected)

    def test_render_cdata_no_text(self):
        calls = [
            LexerCall(command='cdata', data={}),
        ]
        expected = """"""
        self._test_render(calls, expected)


class TestRendererHtml(unittest.TestCase):
    def _test_render(self, calls, expected):
        renderer = RendererHtml()
        output = renderer.render(calls)
        self.assertEqual(expected, output)

    def test_render_cdata(self):
        calls = [
            LexerCall(command='cdata', data={'text': '一段<br><font color="red">文字</font>'}),
        ]
        expected = """一段&lt;br&gt;&lt;font color="red"&gt;文字&lt;/font&gt;"""
        self._test_render(calls, expected)

    def test_render_cdata_no_text(self):
        calls = [
            LexerCall(command='cdata', data={}),
        ]
        expected = """"""
        self._test_render(calls, expected)

    def test_render_htmldata(self):
        calls = [
            LexerCall(command='htmldata', data={'text': '一段<br><font color="red">文字</font>'}),
        ]
        expected = """一段<br><font color="red">文字</font>"""
        self._test_render(calls, expected)

    def test_render_htmldata_no_text(self):
        calls = [
            LexerCall(command='htmldata', data={}),
        ]
        expected = """"""
        self._test_render(calls, expected)

    def test_render_html_start_tag(self):
        calls = [
            LexerCall(
                command='html_start_tag',
                data={
                    'tag': 'font',
                    'attrs': {'color': 'red', 'style': 'cursor: help;'},
                },
            ),
        ]
        expected = """<font color="red" style="cursor: help;">"""
        self._test_render(calls, expected)

    def test_render_html_start_tag_no_attrs(self):
        calls = [
            LexerCall(
                command='html_start_tag',
                data={
                    'tag': 'span',
                },
            ),
        ]
        expected = """<span>"""
        self._test_render(calls, expected)

    def test_render_html_end_tag(self):
        calls = [
            LexerCall(
                command='html_end_tag',
                data={
                    'tag': 'font',
                    'attrs': {'color': 'red', 'style': 'cursor: help;'},
                },
            ),
        ]
        expected = """</font>"""
        self._test_render(calls, expected)

    def test_render_html_end_tag_no_attrs(self):
        calls = [
            LexerCall(
                command='html_end_tag',
                data={
                    'tag': 'span',
                },
            ),
        ]
        expected = """</span>"""
        self._test_render(calls, expected)

    def test_render_html_startend_tag(self):
        calls = [
            LexerCall(
                command='html_startend_tag',
                data={
                    'tag': 'br',
                    'attrs': {'style': 'cursor: help;'},
                },
            ),
        ]
        expected = """<br style="cursor: help;">"""
        self._test_render(calls, expected)

    def test_render_html_startend_tag_no_attrs(self):
        calls = [
            LexerCall(
                command='html_startend_tag',
                data={
                    'tag': 'br',
                },
            ),
        ]
        expected = """<br>"""
        self._test_render(calls, expected)

    def test_render_html_tag(self):
        calls = [
            LexerCall(
                command='html_tag',
                data={
                    'tag': 'font',
                    'attrs': {'color': 'red', 'style': 'cursor: help;'},
                    'text': '一段<br><font color="red">文字</font>',
                },
            ),
        ]
        expected = """<font color="red" style="cursor: help;">一段&lt;br&gt;&lt;font color="red"&gt;文字&lt;/font&gt;</font>"""
        self._test_render(calls, expected)

    def test_render_html_tag_no_attrs(self):
        calls = [
            LexerCall(
                command='html_tag',
                data={
                    'tag': 'span',
                    'text': '一段<br><font color="red">文字</font>',
                },
            ),
        ]
        expected = """<span>一段&lt;br&gt;&lt;font color="red"&gt;文字&lt;/font&gt;</span>"""
        self._test_render(calls, expected)

    def test_render_html_tag_no_text(self):
        calls = [
            LexerCall(
                command='html_tag',
                data={
                    'tag': 'font',
                    'attrs': {'color': 'red', 'style': 'cursor: help;'},
                },
            ),
        ]
        expected = """<font color="red" style="cursor: help;"></font>"""
        self._test_render(calls, expected)

    def test_render_marginalnote_start_start(self):
        calls = [
            LexerCall(command='marginalnote_start', data={'attrs': {'class': '眉批'}}),
        ]
        expected = """<aside class="眉批"><jc-s>〚</jc-s>"""
        self._test_render(calls, expected)

    def test_render_marginalnote_start_end(self):
        calls = [
            LexerCall(command='marginalnote_end', data={'attrs': {'class': '眉批'}}),
        ]
        expected = """<jc-s>〛</jc-s></aside>"""
        self._test_render(calls, expected)

    def test_render_parallel_note_start(self):
        calls = [
            LexerCall(command='parallel_note_start', data={'attrs': {'class': '組排小字'}}),
        ]
        expected = """<small class="組排小字"><jc-s>（</jc-s>"""
        self._test_render(calls, expected)

    def test_render_parallel_note_end(self):
        calls = [
            LexerCall(command='parallel_note_end', data={'attrs': {'class': '組排小字'}}),
        ]
        expected = """<jc-s>）</jc-s></small>"""
        self._test_render(calls, expected)

    def test_render_parallel_note_right_start(self):
        calls = [
            LexerCall(command='parallel_note_right_start', data={'attrs': {'class': '右文'}}),
        ]
        expected = """<span class="右文">"""
        self._test_render(calls, expected)

    def test_render_parallel_note_right_end(self):
        calls = [
            LexerCall(command='parallel_note_right_end', data={'attrs': {'class': '右文'}}),
        ]
        expected = """</span>"""
        self._test_render(calls, expected)

    def test_render_parallel_note_left_start(self):
        calls = [
            LexerCall(command='parallel_note_left_start', data={'attrs': {'class': '左文'}}),
        ]
        expected = """<jc-s>／</jc-s><span class="左文">"""
        self._test_render(calls, expected)

    def test_render_parallel_note_left_end(self):
        calls = [
            LexerCall(command='parallel_note_left_end', data={'attrs': {'class': '左文'}}),
        ]
        expected = """</span>"""
        self._test_render(calls, expected)

    def test_render_cutting_note_start(self):
        calls = [
            LexerCall(command='cutting_note_start', data={'attrs': {'class': '雙行夾注'}}),
        ]
        expected = """<small class="雙行夾注"><jc-s data-rev="古版-元素">（</jc-s>"""
        self._test_render(calls, expected)

    def test_render_cutting_note_end(self):
        calls = [
            LexerCall(command='cutting_note_end', data={'attrs': {'class': '雙行夾注'}}),
        ]
        expected = """<jc-s data-rev="古版-元素">）</jc-s></small>"""
        self._test_render(calls, expected)

    def test_render_footnote_start(self):
        calls = [
            LexerCall(command='footnote_start', data={'attrs': {'class': '腳注'}}),
        ]
        expected = """<small class="腳注"><jc-s>〔</jc-s>"""
        self._test_render(calls, expected)

    def test_render_footnote_end(self):
        calls = [
            LexerCall(command='footnote_end', data={'attrs': {'class': '腳注'}}),
        ]
        expected = """<jc-s>〕</jc-s></small>"""
        self._test_render(calls, expected)

    def test_render_rubynote_start(self):
        calls = [
            LexerCall(command='rubynote_start', data={'attrs': {'class': '旁注'}}),
        ]
        expected = """<small class="旁注"><jc-s>〘</jc-s>"""
        self._test_render(calls, expected)

    def test_render_rubynote_end(self):
        calls = [
            LexerCall(command='rubynote_end', data={'attrs': {'class': '旁注'}}),
        ]
        expected = """<jc-s>〙</jc-s></small>"""
        self._test_render(calls, expected)

    def test_render_whiteonblack_start(self):
        calls = [
            LexerCall(command='whiteonblack_start', data={'attrs': {'class': '陰文'}}),
        ]
        expected = """<b class="陰文"><jc-s>【</jc-s>"""
        self._test_render(calls, expected)

    def test_render_whiteonblack_end(self):
        calls = [
            LexerCall(command='whiteonblack_end', data={'attrs': {'class': '陰文'}}),
        ]
        expected = """<jc-s>】</jc-s></b>"""
        self._test_render(calls, expected)

    def test_render_round_whiteonblack_start(self):
        calls = [
            LexerCall(command='round_whiteonblack_start', data={'attrs': {'class': '圓角陰文'}}),
        ]
        expected = """<b class="圓角陰文"><jc-s>【</jc-s>"""
        self._test_render(calls, expected)

    def test_render_round_whiteonblack_end(self):
        calls = [
            LexerCall(command='round_whiteonblack_end', data={'attrs': {'class': '圓角陰文'}}),
        ]
        expected = """<jc-s>】</jc-s></b>"""
        self._test_render(calls, expected)

    def test_render_bordered_start(self):
        calls = [
            LexerCall(command='bordered_start', data={'attrs': {'class': '方外框'}}),
        ]
        expected = """<b class="方外框"><jc-s>〖</jc-s>"""
        self._test_render(calls, expected)

    def test_render_bordered_end(self):
        calls = [
            LexerCall(command='bordered_end', data={'attrs': {'class': '方外框'}}),
        ]
        expected = """<jc-s>〗</jc-s></b>"""
        self._test_render(calls, expected)

    def test_render_round_bordered_start(self):
        calls = [
            LexerCall(command='round_bordered_start', data={'attrs': {'class': '圓外框'}}),
        ]
        expected = """<b class="圓外框"><jc-s>〖</jc-s>"""
        self._test_render(calls, expected)

    def test_render_round_bordered_end(self):
        calls = [
            LexerCall(command='round_bordered_end', data={'attrs': {'class': '圓外框'}}),
        ]
        expected = """<jc-s>〗</jc-s></b>"""
        self._test_render(calls, expected)

    def test_render_circled_start(self):
        calls = [
            LexerCall(command='circled_start', data={'attrs': {'class': '圓圈'}}),
        ]
        expected = """<b class="圓圈"><jc-s>〖</jc-s>"""
        self._test_render(calls, expected)

    def test_render_circled_end(self):
        calls = [
            LexerCall(command='circled_end', data={'attrs': {'class': '圓圈'}}),
        ]
        expected = """<jc-s>〗</jc-s></b>"""
        self._test_render(calls, expected)

    def test_render_parenthesized_start(self):
        calls = [
            LexerCall(command='parenthesized_start', data={'attrs': {'class': '圓括號'}}),
        ]
        expected = """<b class="圓括號"><jc-s>〖</jc-s>"""
        self._test_render(calls, expected)

    def test_render_parenthesized_end(self):
        calls = [
            LexerCall(command='parenthesized_end', data={'attrs': {'class': '圓括號'}}),
        ]
        expected = """<jc-s>〗</jc-s></b>"""
        self._test_render(calls, expected)


class TestRendererHtmlFilter(unittest.TestCase):
    def _test_render(self, calls, expected, filter=None):
        renderer = RendererHtml(filter=filter)
        output = renderer.render(calls)
        self.assertEqual(expected, output)

    def _test_filter_schema(self, filter, expected):
        """Test whether each call matches the expected output."""
        calls = [
            LexerCall(command='html_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='html_tag', data={'tag': 'span', 'attrs': {'data-rev': '今版'}}),
            LexerCall(command='html_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版', 'class': 'myclass'}}),
            LexerCall(command='html_tag', data={'tag': 'span', 'attrs': {'data-rev': '今版', 'class': 'myclass'}}),
            LexerCall(command='html_tag', data={'tag': 'p', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='html_tag', data={'tag': 'p', 'attrs': {'data-rev': '今版'}}),
        ]

        for i, _ in enumerate(calls):
            test_calls = calls[i:i + 1]
            test_expected = expected[i]
            with self.subTest(i=i, calls=test_calls, expected=test_expected):
                self._test_render(test_calls, test_expected, filter=filter)

    def test_filter_schema_empty_filter(self):
        """Don't match if no filter rule."""
        filter = []
        expected = [
            '<span data-rev="古版"></span>',
            '<span data-rev="今版"></span>',
            '<span data-rev="古版" class="myclass"></span>',
            '<span data-rev="今版" class="myclass"></span>',
            '<p data-rev="古版"></p>',
            '<p data-rev="今版"></p>',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_include_tag(self):
        """Include by tag."""
        filter = [
            {
                'include': [
                    {'tag': 'p'},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '<span data-rev="古版"></span>',
            '<span data-rev="今版"></span>',
            '<span data-rev="古版" class="myclass"></span>',
            '<span data-rev="今版" class="myclass"></span>',
            '',
            '',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_include_attrs(self):
        """Include by all attrs."""
        filter = [
            {
                'include': [
                    {'attrs': {'data-rev': '古版', 'class': 'myclass'}},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '<span data-rev="古版"></span>',
            '<span data-rev="今版"></span>',
            '',
            '<span data-rev="今版" class="myclass"></span>',
            '<p data-rev="古版"></p>',
            '<p data-rev="今版"></p>',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_include_tag_attrs(self):
        """Include by tag and all attrs."""
        filter = [
            {
                'include': [
                    {'tag': 'p', 'attrs': {'data-rev': '古版'}},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '<span data-rev="古版"></span>',
            '<span data-rev="今版"></span>',
            '<span data-rev="古版" class="myclass"></span>',
            '<span data-rev="今版" class="myclass"></span>',
            '',
            '<p data-rev="今版"></p>',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_include_rules(self):
        """Include by any rule."""
        filter = [
            {
                'include': [
                    {'attrs': {'data-rev': '古版'}},
                    {'attrs': {'data-rev': '今版'}},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '',
            '',
            '',
            '',
            '',
            '',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_exclude_tag(self):
        """Exclude by tag."""
        filter = [
            {
                'include': [
                    {'tag': 'span'},
                    {'tag': 'p'},
                ],
                'exclude': [
                    {'tag': 'p'},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '',
            '',
            '',
            '',
            '<p data-rev="古版"></p>',
            '<p data-rev="今版"></p>',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_exclude_attrs(self):
        """Exclude by all attrs."""
        filter = [
            {
                'include': [
                    {'tag': 'span'},
                    {'tag': 'p'},
                ],
                'exclude': [
                    {'attrs': {'data-rev': '古版', 'class': 'myclass'}},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '',
            '',
            '<span data-rev="古版" class="myclass"></span>',
            '',
            '',
            '',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_exclude_tag_attrs(self):
        """Exclude by tag and all attrs."""
        filter = [
            {
                'include': [
                    {'tag': 'span'},
                    {'tag': 'p'},
                ],
                'exclude': [
                    {'tag': 'p', 'attrs': {'data-rev': '古版'}},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '',
            '',
            '',
            '',
            '<p data-rev="古版"></p>',
            '',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_exclude_rules(self):
        """Exclude by any rule."""
        filter = [
            {
                'include': [
                    {'tag': 'span'},
                    {'tag': 'p'},
                ],
                'exclude': [
                    {'attrs': {'data-rev': '古版'}},
                    {'attrs': {'data-rev': '今版'}},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '<span data-rev="古版"></span>',
            '<span data-rev="今版"></span>',
            '<span data-rev="古版" class="myclass"></span>',
            '<span data-rev="今版" class="myclass"></span>',
            '<p data-rev="古版"></p>',
            '<p data-rev="今版"></p>',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_multi_filters_any_work(self):
        """Any filter works."""
        filter = [
            {
                'include': [
                    {'tag': 'span'},
                ],
                'action': 'remove',
            },
            {
                'include': [
                    {'tag': 'p'},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '',
            '',
            '',
            '',
            '',
            '',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_schema_multi_filters_first_win(self):
        """First matched filter wins."""
        filter = [
            {
                'include': [
                    {'tag': 'span'},
                ],
                'action': 'hide_tag',
            },
            {
                'include': [
                    {'tag': 'p'},
                ],
                'action': 'remove',
            },
        ]
        expected = [
            '<jc-t tag="span" attr-data-rev="古版"></jc-t>',
            '<jc-t tag="span" attr-data-rev="今版"></jc-t>',
            '<jc-t tag="span" attr-data-rev="古版" attr-class="myclass"></jc-t>',
            '<jc-t tag="span" attr-data-rev="今版" attr-class="myclass"></jc-t>',
            '',
            '',
        ]
        self._test_filter_schema(filter, expected)

    def test_filter_actions_hide_start_end(self):
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide'},
        ]
        calls = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='cdata', data={'text': '古版'}),
            LexerCall(command='html_startend_tag', data={'tag': 'br', 'attrs': {}}),
            LexerCall(command='cdata', data={'text': '文字'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """<span data-rev="古版" data-jc-innerhtml="古版&lt;br&gt;文字"></span>"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_hide_start_end_empty(self):
        """No data-jc-innerhtml if empty."""
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide'},
        ]
        calls = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """<span data-rev="古版"></span>"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_hide_start_end_nested_hide(self):
        """Nested hide should not be converted."""
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide'},
        ]
        calls = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='cdata', data={'text': '巢狀古版'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """<span data-rev="古版" data-jc-innerhtml="&lt;span data-rev=&quot;古版&quot;&gt;巢狀古版&lt;/span&gt;"></span>"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_hide_start_end_nested_remove(self):
        """Nested remove should work."""
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide'},
            {'include': [{'attrs': {'data-rev': '今版'}}], 'action': 'remove'},
        ]
        calls = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '今版'}}),
            LexerCall(command='cdata', data={'text': '巢狀今版'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '今版'}}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """<span data-rev="古版"></span>"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_hide_startend(self):
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide'},
        ]
        calls = [
            LexerCall(command='html_startend_tag', data={'tag': 'br', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """<br data-rev="古版">"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_remove_start_end(self):
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'remove'},
        ]
        calls = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='cdata', data={'text': '文字'}),
            LexerCall(command='html_startend_tag', data={'tag': 'br', 'attrs': {}}),
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {}}),
            LexerCall(command='cdata', data={'text': '巢狀'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {}}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_remove_startend(self):
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'remove'},
        ]
        calls = [
            LexerCall(command='html_startend_tag', data={'tag': 'br', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_hide_tag_start_end(self):
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide_tag'},
        ]
        calls = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='cdata', data={'text': '文字'}),
            LexerCall(command='html_startend_tag', data={'tag': 'br', 'attrs': {}}),
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='cdata', data={'text': '巢狀古版'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """<jc-t tag="span" attr-data-rev="古版">文字<br><jc-t tag="span" attr-data-rev="古版">巢狀古版</jc-t></jc-t>"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_hide_tag_startend(self):
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'hide_tag'},
        ]
        calls = [
            LexerCall(command='html_startend_tag', data={'tag': 'br', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """<jc-t tag="br" attr-data-rev="古版"></jc-t>"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_remove_tag_start_end(self):
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'remove_tag'},
        ]
        calls = [
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='cdata', data={'text': '文字'}),
            LexerCall(command='html_startend_tag', data={'tag': 'br', 'attrs': {}}),
            LexerCall(command='html_start_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='cdata', data={'text': '巢狀古版'}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
            LexerCall(command='html_end_tag', data={'tag': 'span', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """文字<br>巢狀古版"""
        self._test_render(calls, expected, filter=filter)

    def test_filter_actions_remove_tag_startend(self):
        filter = [
            {'include': [{'attrs': {'data-rev': '古版'}}], 'action': 'remove_tag'},
        ]
        calls = [
            LexerCall(command='html_startend_tag', data={'tag': 'br', 'attrs': {'data-rev': '古版'}}),
        ]
        expected = """"""
        self._test_render(calls, expected, filter=filter)
