import json
import os
import tempfile
import unittest
from textwrap import dedent
from time import sleep
from unittest import mock

from jicheng import fchecksum
from jicheng.generate import StaticSiteGenerator

from ... import TEMP_DIR


def setUpModule():
    """Set up a temp directory for testing."""
    global _tmpdir, tmpdir
    _tmpdir = tempfile.TemporaryDirectory(prefix='gen-', dir=TEMP_DIR)
    tmpdir = _tmpdir.name


def tearDownModule():
    """Cleanup the temp directory."""
    _tmpdir.cleanup()


class TestGenerateBase(unittest.TestCase):
    def setUp(self):
        """Common setup."""
        self.maxDiff = None

    def _test_run(self, expected=None, expected_cache=None):
        """Check if we get expected file status and cache."""
        def check_file_stat(file, key, value):
            if key == 'exists':
                self.assertIs(value, os.path.exists(file))

            elif key == 'isfile':
                self.assertIs(value, os.path.isfile(file))

            elif key == 'isdir':
                self.assertIs(value, os.path.isdir(file))

            elif key == 'text':
                with open(file, 'r', encoding='UTF-8') as fh:
                    self.assertEqual(value, fh.read())

            elif key == 'mtime':
                self.assertEqual(value, os.path.getmtime(file))

            elif key == 'old_mtime':
                self.assertLess(value, os.path.getmtime(file))

            else:
                raise KeyError(f'Unsupported file stat: "{key}"')

        if expected is not None:
            for file, stat in expected.items():
                with self.subTest(file=file, stat=stat):
                    for k, v in stat.items():
                        check_file_stat(file, k, v)

        if expected_cache is not None:
            with self.subTest(cache=expected_cache):
                with open(self.fcache, 'r', encoding='UTF-8') as fh:
                    self.assertEqual(expected_cache, json.load(fh))


class TestGenerateSite(TestGenerateBase):
    """測試站台生成運作。"""

    def setUp(self):
        """Set up data files for testing."""
        super().setUp()

        self.root = tempfile.mkdtemp(dir=tmpdir)
        self.source = os.path.join(self.root, 'pages')
        self.public = os.path.join(self.root, 'public')

        self.fcache = os.path.join(self.root, 'cache.json')
        self.ftpl = os.path.join(self.root, 'themes', 'default', 'template', 'html', 'base.html')
        self.fsrcx1 = os.path.join(self.root, 'themes', 'default', 'static', 'style.css')
        self.fsrc1 = os.path.join(self.source, 'index.html')
        self.fsrc2 = os.path.join(self.source, 'subdir', 'index.html')
        self.fsrc3 = os.path.join(self.source, 'subdir', 'page.html')
        self.fdstx1 = os.path.join(self.public, 'style.css')
        self.fdst1 = os.path.join(self.public, 'index.html')
        self.fdst2 = os.path.join(self.public, 'subdir', 'index.html')
        self.fdst3 = os.path.join(self.public, 'subdir', 'page.html')

        os.makedirs(os.path.dirname(self.ftpl), exist_ok=True)
        with open(self.ftpl, 'w', encoding='UTF-8') as fh:
            fh.write(
                dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1>
                    {%- for title, url in get_breadcrumbs() -%}
                      {%- if not loop.first %} » {% endif -%}
                      {%- if url is not none -%}
                        <a href="{{ url }}">{{ title }}</a>
                      {%- else -%}
                        {{ title }}
                      {%- endif -%}
                    {%- endfor -%}
                    </h1>
                    {{ body|safe }}
                    </body>"""
                )
            )

        os.makedirs(os.path.dirname(self.fsrc1), exist_ok=True)
        with open(self.fsrc1, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>Index</h1></header>""")

        os.makedirs(os.path.dirname(self.fsrc2), exist_ok=True)
        with open(self.fsrc2, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>Dir</h1></header>""")

        os.makedirs(os.path.dirname(self.fsrc3), exist_ok=True)
        with open(self.fsrc3, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>Page</h1></header>""")

        os.makedirs(os.path.dirname(self.fsrcx1), exist_ok=True)
        with open(self.fsrcx1, 'w', encoding='UTF-8') as fh:
            fh.write("""blockquote { background-color: gray; }""")

    @mock.patch('jicheng.generate.log')
    def test_run_01(self, _):
        """Basic"""
        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>"""
                ),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_02(self, _):
        """Test if page generated before index."""
        os.remove(self.fsrc3)
        self.fsrc3 = os.path.join(self.source, 'subdir', 'another.html')
        self.fdst3 = os.path.join(self.public, 'subdir', 'another.html')

        os.makedirs(os.path.dirname(self.fsrc3), exist_ok=True)
        with open(self.fsrc3, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>Page</h1></header>""")

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>"""
                ),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="another.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_no_page(self, _):
        """No public dir and cache file if no page exists."""
        os.remove(self.fsrcx1)
        os.remove(self.fsrc1)
        os.remove(self.fsrc2)
        os.remove(self.fsrc3)

        expected = {
            self.fdstx1: {
                'exists': False,
            },
            self.fdst1: {
                'exists': False,
            },
            self.fdst2: {
                'exists': False,
            },
            self.fdst3: {
                'exists': False,
            },
            os.path.dirname(self.fdst1): {
                'exists': False,
            },
            os.path.dirname(self.fdst2): {
                'exists': False,
            },
            os.path.dirname(self.fdst3): {
                'exists': False,
            },
            self.public: {
                'exists': False,
            },
            self.fcache: {
                'exists': False,
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected)

    @mock.patch('jicheng.generate.log')
    def test_run_content_change(self, _):
        """Regenerate output page and update cache if source file content changes."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        with open(self.fsrc3, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>PageNew</h1></header>""")

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="page.html">PageNew</a></h1>
                    <header><h1>PageNew</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_mtime_change(self, _):
        """Only update cache if source file mtime changes."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.utime(self.fsrc3)

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'mtime': os.path.getmtime(self.fdst3),
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_removed(self, _):
        """Remove output page file and remove from cache if source file removed."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.remove(self.fsrc3)

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'exists': False,
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_remove_stale_dir(self, _):
        """Remove empty dir if containing files no longer exist."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.remove(self.fsrc2)
        os.remove(self.fsrc3)

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'exists': False,
            },
            self.fdst3: {
                'exists': False,
            },
            os.path.dirname(self.fdst2): {
                'exists': False,
            },
            os.path.dirname(self.fdst3): {
                'exists': False,
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_template_change(self, _):
        """Regenerate all output pages and update cache if template changes."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        with open(self.ftpl, 'w', encoding='UTF-8') as fh:
            fh.write(
                dedent(
                    """\
                    <!DOCTYPE html>
                    <html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1>
                    {%- for title, url in get_breadcrumbs() -%}
                      {%- if not loop.first %} » {% endif -%}
                      {%- if url is not none -%}
                        <a href="{{ url }}">{{ title }}</a>
                      {%- else -%}
                        {{ title }}
                      {%- endif -%}
                    {%- endfor -%}
                    </h1>
                    {{ body|safe }}
                    </body>
                    </html>"""
                )
            )

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>
                    </html>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>
                    </html>"""
                ),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>
                    </html>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_template_mtime_change(self, _):
        """Only update cache if template mtime changes."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.utime(self.ftpl)

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'mtime': os.path.getmtime(self.fdst3),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_any_template_change(self, _):
        """Regenerate all output pages and update cache if any template changes."""
        ftpl2 = os.path.join(self.root, 'themes', 'default', 'template', 'html', 'page.html')
        with open(ftpl2, 'w', encoding='UTF-8') as fh:
            fh.write("""dummy""")

        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        with open(ftpl2, 'w', encoding='UTF-8') as fh:
            fh.write("""dummy2""")

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'old_mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'old_mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'old_mtime': os.path.getmtime(self.fdst3),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(ftpl2, self.root): {
                    'hash': fchecksum(ftpl2),
                    'mtime': os.path.getmtime(ftpl2),
                    'size': os.path.getsize(ftpl2),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_upper_change(self, _):
        """Regenerate output page and update cache if an upper page changes."""
        StaticSiteGenerator(self.root).run()

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'old_mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">DirNew</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        sleep(1E-6)
        with open(self.fsrc2, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>DirNew</h1></header>""")

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_upper_mtime_change(self, _):
        """Only update cache if an upper page mtime changes."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.utime(self.fsrc2)

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'mtime': os.path.getmtime(self.fdst3),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_upper_removed(self, _):
        """Regenerate output page and update cache if an upper page removed."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.remove(self.fsrc2)

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'exists': False,
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » subdir » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_static_content_change(self, _):
        """Regenerate static file and update cache if source static file content changes."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        with open(self.fsrcx1, 'w', encoding='UTF-8') as fh:
            fh.write("""p { color: red; }""")

        expected = {
            self.fdstx1: {
                'text': """p { color: red; }""",
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'mtime': os.path.getmtime(self.fdst3),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_static_mtime_change(self, _):
        """Only update cache if source static file mtime changes."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.utime(self.fsrcx1)

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'mtime': os.path.getmtime(self.fdst3),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_static_removed(self, _):
        """Remove output file and remove from cache if source static file removed."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.remove(self.fsrcx1)

        expected = {
            self.fdstx1: {
                'exists': False,
            },
            self.fdst1: {
                'mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'mtime': os.path.getmtime(self.fdst3),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_static_page_no_info(self, _):
        """No info for static page."""
        os.remove(self.fsrc2)

        self.fsrcx2 = os.path.join(self.root, 'themes', 'default', 'static', 'subdir', 'index.html')
        os.makedirs(os.path.dirname(self.fsrcx2), exist_ok=True)
        with open(self.fsrcx2, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>StaticPage</h1></header>""")

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': """<header><h1>StaticPage</h1></header>""",
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">subdir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrcx2, self.root): {
                    'hash': fchecksum(self.fsrcx2),
                    'mtime': os.path.getmtime(self.fsrcx2),
                    'size': os.path.getsize(self.fsrcx2),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_static_page_upper_content_change(self, _):
        """Regenerate output page and update cache if an upper static page changed."""
        os.remove(self.fsrc2)

        self.fsrcx2 = os.path.join(self.root, 'themes', 'default', 'static', 'subdir', 'index.html')
        os.makedirs(os.path.dirname(self.fsrcx2), exist_ok=True)
        with open(self.fsrcx2, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>StaticPage</h1></header>""")

        StaticSiteGenerator(self.root).run()

        with open(self.fsrcx2, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>StaticPage2</h1></header>""")

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': """<header><h1>StaticPage2</h1></header>""",
            },
            self.fdst3: {
                'old_mtime': os.path.getmtime(self.fdst3),
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">subdir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrcx2, self.root): {
                    'hash': fchecksum(self.fsrcx2),
                    'mtime': os.path.getmtime(self.fsrcx2),
                    'size': os.path.getsize(self.fsrcx2),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_static_page_upper_removed(self, _):
        """Regenerate page if an upper static page removed."""
        os.remove(self.fsrc2)

        self.fsrcx2 = os.path.join(self.root, 'themes', 'default', 'static', 'subdir', 'index.html')
        os.makedirs(os.path.dirname(self.fsrcx2), exist_ok=True)
        with open(self.fsrcx2, 'w', encoding='UTF-8') as fh:
            fh.write("""<header><h1>StaticPage</h1></header>""")

        StaticSiteGenerator(self.root).run()

        os.remove(self.fsrcx2)

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'exists': False,
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » subdir » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_coexist(self, _):
        """Take source static file in prior if source page also exists."""
        self.fsrcx2 = os.path.join(self.root, 'themes', 'default', 'static', 'subdir', 'page.html')
        os.makedirs(os.path.dirname(self.fsrcx2), exist_ok=True)
        with open(self.fsrcx2, 'w', encoding='UTF-8') as fh:
            fh.write("""<p>StaticPage</p>""")

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>"""
                ),
            },
            self.fdst3: {
                'text': """<p>StaticPage</p>""",
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrcx2, self.root): {
                    'hash': fchecksum(self.fsrcx2),
                    'mtime': os.path.getmtime(self.fsrcx2),
                    'size': os.path.getsize(self.fsrcx2),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_coexist_file_added(self, _):
        """Take source static file in prior if added."""
        StaticSiteGenerator(self.root).run()

        self.fsrcx2 = os.path.join(self.root, 'themes', 'default', 'static', 'subdir', 'page.html')
        os.makedirs(os.path.dirname(self.fsrcx2), exist_ok=True)
        with open(self.fsrcx2, 'w', encoding='UTF-8') as fh:
            fh.write("""<p>StaticPage</p>""")

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>"""
                ),
            },
            self.fdst3: {
                'text': """<p>StaticPage</p>""",
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrcx2, self.root): {
                    'hash': fchecksum(self.fsrcx2),
                    'mtime': os.path.getmtime(self.fsrcx2),
                    'size': os.path.getsize(self.fsrcx2),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_coexist_file_removed(self, _):
        """Take source page if coexisted static file removed."""
        self.fsrcx2 = os.path.join(self.root, 'themes', 'default', 'static', 'subdir', 'page.html')
        os.makedirs(os.path.dirname(self.fsrcx2), exist_ok=True)
        with open(self.fsrcx2, 'w', encoding='UTF-8') as fh:
            fh.write("""<p>StaticPage</p>""")

        StaticSiteGenerator(self.root).run()

        os.remove(self.fsrcx2)

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>"""
                ),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_regen_dst_extra(self, _):
        """Remove extra files and directories."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        self.fdst4 = os.path.join(self.public, 'subdir2', 'page2.html')
        os.makedirs(os.path.dirname(self.fdst4), exist_ok=True)
        with open(self.fdst4, 'w', encoding='UTF-8') as fh:
            fh.write("""Extra page.""")

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>"""
                ),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
            os.path.dirname(self.fdst4): {
                'exists': False,
            },
            self.fdst4: {
                'exists': False,
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_regen_dst_removed(self, _):
        """Regenerate output file(s) if output file(s) missing."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.remove(self.fdstx1)
        os.remove(self.fdst1)
        os.remove(self.fdst2)
        os.remove(self.fdst3)

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>"""
                ),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_regen_dst_occupied_by_dir(self, _):
        """Regenerate output file(s) even if occupied by dir."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.remove(self.fdstx1)
        os.remove(self.fdst1)
        os.remove(self.fdst2)
        os.remove(self.fdst3)
        os.makedirs(self.fdstx1, exist_ok=True)
        os.makedirs(self.fdst1, exist_ok=True)
        os.makedirs(self.fdst2, exist_ok=True)
        os.makedirs(self.fdst3, exist_ok=True)

        expected = {
            self.fdstx1: {
                'text': """blockquote { background-color: gray; }""",
            },
            self.fdst1: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="index.html">Index</a></h1>
                    <header><h1>Index</h1></header>
                    </body>"""
                ),
            },
            self.fdst2: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a></h1>
                    <header><h1>Dir</h1></header>
                    </body>"""
                ),
            },
            self.fdst3: {
                'text': dedent(
                    """\
                    <!DOCTYPE html>
                    <head>
                    <meta charset="UTF-8">
                    </head>
                    <body>
                    <h1><a href="../index.html">Index</a> » <a href="index.html">Dir</a> » <a href="page.html">Page</a></h1>
                    <header><h1>Page</h1></header>
                    </body>"""
                ),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_regen_dst_change(self, _):
        """Don't regenerate output file(s) even if output file(s) changes."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        with open(self.fdstx1, 'w', encoding='UTF-8') as fh:
            fh.write("""fdstx1""")

        with open(self.fdst1, 'w', encoding='UTF-8') as fh:
            fh.write("""fdst1""")

        os.makedirs(os.path.dirname(self.fsrc2), exist_ok=True)
        with open(self.fdst2, 'w', encoding='UTF-8') as fh:
            fh.write("""fdst2""")

        os.makedirs(os.path.dirname(self.fsrc3), exist_ok=True)
        with open(self.fdst3, 'w', encoding='UTF-8') as fh:
            fh.write("""fdst3""")

        expected = {
            self.fdstx1: {
                'text': """fdstx1""",
            },
            self.fdst1: {
                'text': """fdst1""",
            },
            self.fdst2: {
                'text': """fdst2""",
            },
            self.fdst3: {
                'text': """fdst3""",
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_regen_all_if_forced(self, _):
        """Regenerate all pages if forced."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)

        expected = {
            self.fdstx1: {
                'mtime': os.path.getmtime(self.fdstx1),
            },
            self.fdst1: {
                'old_mtime': os.path.getmtime(self.fdst1),
            },
            self.fdst2: {
                'old_mtime': os.path.getmtime(self.fdst2),
            },
            self.fdst3: {
                'old_mtime': os.path.getmtime(self.fdst3),
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {
                os.path.relpath(self.ftpl, self.root): {
                    'hash': fchecksum(self.ftpl),
                    'mtime': os.path.getmtime(self.ftpl),
                    'size': os.path.getsize(self.ftpl),
                },
                os.path.relpath(self.fsrcx1, self.root): {
                    'hash': fchecksum(self.fsrcx1),
                    'mtime': os.path.getmtime(self.fsrcx1),
                    'size': os.path.getsize(self.fsrcx1),
                },
                os.path.relpath(self.fsrc1, self.root): {
                    'hash': fchecksum(self.fsrc1),
                    'mtime': os.path.getmtime(self.fsrc1),
                    'size': os.path.getsize(self.fsrc1),
                },
                os.path.relpath(self.fsrc2, self.root): {
                    'hash': fchecksum(self.fsrc2),
                    'mtime': os.path.getmtime(self.fsrc2),
                    'size': os.path.getsize(self.fsrc2),
                },
                os.path.relpath(self.fsrc3, self.root): {
                    'hash': fchecksum(self.fsrc3),
                    'mtime': os.path.getmtime(self.fsrc3),
                    'size': os.path.getsize(self.fsrc3),
                },
            },
        }

        StaticSiteGenerator(self.root).run(force=True)

        self._test_run(expected, expected_cache)

    @mock.patch('jicheng.generate.log')
    def test_run_regen_clear_cache_and_public_if_no_page(self, _):
        """Remove public dir and clear cache if no page exists."""
        StaticSiteGenerator(self.root).run()

        sleep(1E-6)
        os.remove(self.fsrcx1)
        os.remove(self.fsrc1)
        os.remove(self.fsrc2)
        os.remove(self.fsrc3)

        expected = {
            self.fdstx1: {
                'exists': False,
            },
            self.fdst1: {
                'exists': False,
            },
            self.fdst2: {
                'exists': False,
            },
            self.fdst3: {
                'exists': False,
            },
            os.path.dirname(self.fdst1): {
                'exists': False,
            },
            os.path.dirname(self.fdst2): {
                'exists': False,
            },
            os.path.dirname(self.fdst3): {
                'exists': False,
            },
            self.public: {
                'exists': False,
            },
        }

        expected_cache = {
            'schema': 2,
            'src': {},
        }

        StaticSiteGenerator(self.root).run()

        self._test_run(expected, expected_cache)
