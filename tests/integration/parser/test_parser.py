import io
import unittest

from jicheng.syntaxparser import Parser

from ... import parametrize


class TestParserHtml(unittest.TestCase):
    """測試主要笈成語法的輸入與輸出。"""

    def _test_run(self, html, expected, char_subst_table=None, renderer_filter=None):
        parser = Parser(
            char_subst_table=char_subst_table,
            renderer_filter=renderer_filter,
        )
        with io.BytesIO(html.encode('UTF-8')) as fh:
            parser.run(fh, imode='html', omode='html')
        self.assertEqual(expected, parser.output)

    def _test_run_book(self, html, expected,
                       book_only=True,
                       data_type='book',
                       char_subst_table=None,
                       renderer_filter=None,
                       ):
        """Test for data-type="book"."""
        attr = f' data-type="{data_type}"' if data_type else ''
        html = f"""<header{attr}></header>""" + html

        if book_only and data_type != 'book':
            expected = html
        else:
            expected = f"""<header{attr}></header>""" + expected

        self._test_run(
            html, expected,
            char_subst_table=char_subst_table,
            renderer_filter=renderer_filter,
        )

    @parametrize('data-type', ('book', 'other', None))
    @parametrize(
        'msg, html, expected, book_only',
        (
            (
                """p""",
                """<p>text</p>""",
                """<div data-sec="p">text</div>""",
                True,
            ),
            (
                """ins""",
                """<ins>text</ins>""",
                """<span data-rev="今版">text</span>""",
                True,
            ),
            (
                """del""",
                """<del>text</del>""",
                """<span data-rev="古版">text</span>""",
                True,
            ),
            (
                """aside.眉批""",
                """<aside class="眉批">text</aside>""",
                """<aside class="眉批"><jc-s>〚</jc-s>text<jc-s>〛</jc-s></aside>""",
                True,
            ),
            (
                """small.組排小字""",
                """<small class="foo 組排小字 bar">text</small>""",
                """<small class="foo 組排小字 bar"><jc-s>（</jc-s>text<jc-s>）</jc-s></small>""",
                True,
            ),
            (
                """small.組排小字 with (span.右文, span.左文)""",
                """<small class="foo 組排小字 bar">"""
                """<span class="foo 右文 bar">textR</span>"""
                """<span class="foo 左文 bar">textL</span>"""
                """</small>""",
                """<small class="foo 組排小字 bar">"""
                """<jc-s>（</jc-s>"""
                """<span class="foo 右文 bar">textR</span>"""
                """<jc-s>／</jc-s>"""
                """<span class="foo 左文 bar">textL</span>"""
                """<jc-s>）</jc-s>"""
                """</small>""",
                True,
            ),
            (
                """Bare (span.右文, span.左文) should not be parsed""",
                """<span class="foo 右文 bar">textR</span>"""
                """<span class="foo 左文 bar">textL</span>""",
                """<span class="foo 右文 bar">textR</span>"""
                """<span class="foo 左文 bar">textL</span>""",
                False,
            ),
            (
                """small.雙行夾注""",
                """<small class="foo 雙行夾注 bar">text</small>""",
                """<small class="foo 雙行夾注 bar"><jc-s data-rev="古版-元素">（</jc-s>text<jc-s data-rev="古版-元素">）</jc-s></small>""",
                True,
            ),
            (
                """small.腳注""",
                """<small class="foo 腳注 bar">text</small>""",
                """<small class="foo 腳注 bar"><jc-s>〔</jc-s>text<jc-s>〕</jc-s></small>""",
                True,
            ),
            (
                """small.旁注""",
                """<small class="foo 旁注 bar">text</small>""",
                """<small class="foo 旁注 bar"><jc-s>〘</jc-s>text<jc-s>〙</jc-s></small>""",
                True,
            ),
            (
                """b.陰文""",
                """<b class="foo 陰文 bar">text</b>""",
                """<b class="foo 陰文 bar"><jc-s>【</jc-s>text<jc-s>】</jc-s></b>""",
                True,
            ),
            (
                """b.圓角陰文""",
                """<b class="foo 圓角陰文 bar">text</b>""",
                """<b class="foo 圓角陰文 bar"><jc-s>【</jc-s>text<jc-s>】</jc-s></b>""",
                True,
            ),
            (
                """b.方外框""",
                """<b class="foo 方外框 bar">text</b>""",
                """<b class="foo 方外框 bar"><jc-s>〖</jc-s>text<jc-s>〗</jc-s></b>""",
                True,
            ),
            (
                """b.圓外框""",
                """<b class="foo 圓外框 bar">text</b>""",
                """<b class="foo 圓外框 bar"><jc-s>〖</jc-s>text<jc-s>〗</jc-s></b>""",
                True,
            ),
            (
                """b.圓圈""",
                """<b class="foo 圓圈 bar">text</b>""",
                """<b class="foo 圓圈 bar"><jc-s>〖</jc-s>text<jc-s>〗</jc-s></b>""",
                True,
            ),
            (
                """b.圓括號""",
                """<b class="foo 圓括號 bar">text</b>""",
                """<b class="foo 圓括號 bar"><jc-s>〖</jc-s>text<jc-s>〗</jc-s></b>""",
                True,
            ),
        ),
    )
    def test_run_book(self, msg, html, expected, book_only, data_type):
        """Test for data-type="book"."""
        self._test_run_book(
            html, expected,
            book_only=book_only,
            data_type=data_type,
        )

    @parametrize('data-type', ('book', 'other', None))
    @parametrize(
        'msg, html, expected, book_only',
        (
            (
                """Ancient/Modern""",
                """温病""",
                """<span data-rev="古版">温</span><span data-rev="今版">溫</span>病""",
                True,
            ),
            (
                """Modern""",
                """一段文字，下一段""",
                """一段文字<span data-rev="今版">，</span>下一段""",
                True,
            ),
            (
                """Ancient""",
                """芍藥○枳實""",
                """芍藥<span data-rev="古版">○</span>枳實""",
                True,
            ),
            (
                """Merge""",
                """室内，温病""",
                """室<span data-rev="古版">内</span><span data-rev="今版">內，</span><span data-rev="古版">温</span><span data-rev="今版">溫</span>病""",
                True,
            ),
            (
                """Can't merge""",
                """内温""",
                """<span data-rev="古版">内</span><span data-rev="今版">內</span><span data-rev="古版">温</span><span data-rev="今版">溫</span>""",
                True,
            ),
            (
                """Skip conversion""",
                """<del>内温</del>""",
                """<span data-rev="古版">内温</span>""",
                True,
            ),
            (
                """Skip conversion""",
                """<ins>内温</ins>""",
                """<span data-rev="今版">内温</span>""",
                True,
            ),
            (
                """Skip conversion""",
                """<span data-rev="古版">内温</span>""",
                """<span data-rev="古版">内温</span>""",
                True,
            ),
            (
                """Skip conversion""",
                """<span data-rev="今版">内温</span>""",
                """<span data-rev="今版">内温</span>""",
                True,
            ),
        ),
    )
    def test_run_book_01(self, msg, html, expected, book_only, data_type):
        """Test for data-type="book" with char_subst_table."""
        char_subst_table = {
            '，': {'今': '，'},
            '、': {'今': '、'},
            '。': {'今': '。'},
            '○': {'古': '○'},
            '内': {'古': '内', '今': '內'},
            '温': {'古': '温', '今': '溫'},
        }

        self._test_run_book(
            html, expected,
            book_only=book_only,
            data_type=data_type,
            char_subst_table=char_subst_table,
        )

    @parametrize('data-type', ('book', 'other', None))
    @parametrize(
        'msg, html, expected, book_only',
        (
            (
                """Basic""",
                """<span data-rev="古版">温</span><span data-rev="今版">溫</span>病""",
                """<span data-rev="古版" data-jc-innerhtml="温"></span><span data-rev="今版">溫</span>病""",
                False,
            ),
        ),
    )
    def test_run_book_02(self, msg, html, expected, book_only, data_type):
        """Test for data-type="book" with renderer_filter."""
        renderer_filter = [
            {
                'include': [
                    {'attrs': {'data-rev': '古版'}},
                ],
                'action': 'hide',
            },
        ]

        self._test_run_book(
            html, expected,
            book_only=book_only,
            data_type=data_type,
            renderer_filter=renderer_filter,
        )

    @parametrize('data-type', ('book', 'other', None))
    @parametrize(
        'msg, html, expected, book_only',
        (
            (
                """Basic""",
                """温病""",
                """<span data-rev="古版" data-jc-innerhtml="温"></span><span data-rev="今版">溫</span>病""",
                True,
            ),
        ),
    )
    def test_run_book_03(self, msg, html, expected, book_only, data_type):
        """Test for data-type="book" with char_subst_table and renderer_filter."""
        char_subst_table = {
            '内': {'古': '内', '今': '內'},
            '温': {'古': '温', '今': '溫'},
        }
        renderer_filter = [
            {
                'include': [
                    {'attrs': {'data-rev': '古版'}},
                ],
                'action': 'hide',
            },
        ]

        self._test_run_book(
            html,
            expected,
            char_subst_table=char_subst_table,
            renderer_filter=renderer_filter,
        )
