#!/usr/bin/env python3
from setuptools import find_packages, setup

with open('README.md', encoding='UTF-8') as fh:
    long_description = fh.read()

setup(
    name='jicheng',
    version='0.1.0',
    description='A static site generator that supports HTML based contents.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Danny Lin',
    author_email='danny0838@gmail.com',
    url='https://gitlab.com/jicheng/jc.ssg',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Operating System :: OS Independent',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Topic :: Internet :: WWW/HTTP :: Site Management',
        'Topic :: Text Processing :: Markup :: HTML',
    ],
    python_requires='~=3.7',
    install_requires=[
        'lxml >= 4.4',
        'pyyaml >= 5.0',
        'jinja2 >= 3.0',
        'cached_property >= 1.5.0; python_version < "3.8"',
    ],
    extras_require={
        'dev': [
            'flake8 >= 4.0',
            'pep8-naming >= 0.13.2',
            'flake8-comprehensions >= 3.7',
            'flake8-string-format >= 0.3',
            'flake8-quotes >= 3.0',
            'flake8-bugbear >= 22.0',
            'flake8-isort >= 4.2',
            'isort >= 5.5',
        ],
    },
    packages=find_packages(exclude=['tests']),
    entry_points={
        'console_scripts': [
            'jicheng = jicheng.cli:main',
        ],
    },
)
