# 笈成站台產生器

笈成站台產生器是把笈成資料原始檔轉為靜態站台的腳本程式。

## 操作方法

### 1. 安裝笈成站台產生器
1. 安裝 Python 3.7 以上版本。一般可至 [Python 官網](https://www.python.org/)下載，進階使用者可視需求安裝[其他的 Python 實做](https://www.python.org/download/alternatives/)。
2. 下載[最新版本的笈成站台產生器原始檔](https://gitlab.com/jicheng/jc.ssg)，於命令列進入主目錄（含有 `setup.py` 者）後執行 `pip install .`。
3. 如要更新笈成站台產生器，可先執行 `pip uninstall jicheng` 反安裝，再按上述步驟重新安裝下載的原始檔。

### 2. 執行笈成站台產生器
* 下載最新版本的[笈成資料檔](https://gitlab.com/jicheng/jc.data)，於命令列進入笈成資料根目錄（含有 `config.yaml` 者）後執行 `jicheng g`，即會自動產生靜態站台。
* 其他操作細節可於命令列執行 `jicheng --help` 查看操作手冊。

### 3. 檢視及操作靜態站台
笈成站台產生器產生的靜態站台（預設輸出到 `<笈成資料根目錄>/public/`）可直接用瀏覽器瀏覽，開啟靜態站台主目錄下的 `index.html` 即可進入瀏覽笈成站台。

從本機開啟產生的網頁時，大部分功能皆能正常運作，但由於現代瀏覽器大多基於安全性考量預設禁止腳本存取本地資料，會導致使用相關技術的功能無法正常運作，如典籍檢索器。主要解決方法有二：

其一是架設伺服器再透過瀏覽器存取。可把伺服器架在本機，或把資料上傳到支援網站架設的伺服器。

> 若要架設簡易的本地伺服器，可用 Python 內建的伺服器模組：在命令列進入資料檔目錄後執行 `python -m http.server`，然後於瀏覽器網址列輸入 `http://localhost:8000` 即可進入站台（可視需求更改 port 或 IP，詳情可執行 `python -m http.server --help` 參閱命令說明）。

另一做法是調整瀏覽器設定以解除限制。但這麼做有安全風險，不建議日常上網的瀏覽器這樣做，宜於用過後回復原設定，或安裝另一獨立瀏覽器做此設定：

* Chromium 系列（包括 Google Chrome、79 版以後的 Edge 等）: 建立瀏覽器應用程式的捷徑，在執行檔路徑後加上參數 ` --allow-file-access-from-files`。
* Firefox: 於網址列輸入 `about:config`，搜尋 `security.fileuri.strict_origin_policy` 將值改為 `false`。

### 4. 進階設定
可更改 `<笈成資料根目錄>/config.yaml` 調整笈成站台產生器的設定。詳見[笈成資料的說明文件](https://gitlab.com/jicheng/jc.data)。

再次執行產生器時，會自動偵測更動的檔案並重新產生受影響的頁面，但更動設定檔時產生器無法判斷受影響的頁面，預設不會重新產生。可刪除輸出目錄下需要更新的檔案或強制重新產生所有頁面（`jicheng g --force`）使新設定生效。
